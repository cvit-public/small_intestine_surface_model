import pandas as pd

data = {
    'case': ['Patient_0'],
    'start_point': [[322,258,408]],
    'end_point': [[165,207,146]]
}

df = pd.DataFrame(data)

df.to_parquet('./start_end.parquet')

# OLD STUFF BELOW

# import trimesh
# import numpy as np
# import pandas as pd
# from pathlib import Path

# # Directory with folders of STL files
# main_dir_path = Path("./data")

# # DataFrame to store the results
# data = {
#     'case': [],
#     'start_point': [],
#     'end_point': [],
# }

# # Iterate over each subdirectory
# for dir_path in main_dir_path.iterdir():
#     if dir_path.is_dir():
#         # Get all STL files in the directory
#         stl_files = [f for f in dir_path.iterdir() if f.suffix == ".stl"]

#         # Search for the STL files that have 'colon', 'small_bowel', and 'duodenum' in their names
#         colon_stl = next((f for f in stl_files if 'colon' in f.name), None)
#         small_bowel_stl = next((f for f in stl_files if 'small_bowel' in f.name), None)
#         duodenum_stl = next((f for f in stl_files if 'duodenum' in f.name), None)

#         if not colon_stl or not small_bowel_stl or not duodenum_stl:
#             print(f"Could not find all the necessary STL files in folder {dir_path.name}")
#             continue

#         # Load the meshes from STL files
#         colon_mesh = trimesh.load_mesh(colon_stl)
#         small_bowel_mesh = trimesh.load_mesh(small_bowel_stl)
#         duodenum_mesh = trimesh.load_mesh(duodenum_stl)


#         try:
#             # Find the intersection of the colon and small bowel meshes
#             intersection_colon_small_bowel = trimesh.boolean.intersection([colon_mesh, small_bowel_mesh], engine='blender')
#             if intersection_colon_small_bowel.is_empty:
#                 print(f"No intersection between the colon and small bowel meshes in folder {dir_path.name}")
#                 end_point = None
#             else:
#                 # Find the point with the lowest weighted x (2/3) and z (1/3) coordinate
#                 end_point = intersection_colon_small_bowel.vertices[np.argmin(2/3*intersection_colon_small_bowel.vertices[:, 0] + 1/3*intersection_colon_small_bowel.vertices[:, 2])]
#                 print(f"End Point in the intersection of colon and small bowel meshes in folder {dir_path.name}: {end_point}")

#         # if We fail to load the scene, take it as None
#         except Exception as e:
#             print(f'folder{dir_path} - {e}')
#             end_point = None


#         # Find the intersection of the small bowel and duodenum meshes
#         intersection_small_bowel_duodenum = trimesh.boolean.intersection([small_bowel_mesh, duodenum_mesh], engine='blender')

#         if intersection_small_bowel_duodenum.is_empty:
#             print(f"No intersection between the small bowel and duodenum meshes in folder {dir_path.name}")
#             start_point = None
#         else:
#             # Find the point with the highest x coordinate
#             start_point = intersection_small_bowel_duodenum.vertices[np.argmax(intersection_small_bowel_duodenum.vertices[:, 0])]
#             print(f"Start Point in the intersection of small bowel and duodenum meshes in folder {dir_path.name}: {start_point}")

#         # Append the results to the DataFrame
#         data['case'].append(dir_path.name)
#         data['start_point'].append(start_point)
#         data['end_point'].append(end_point)

# # Save the DataFrame to a CSV file
# df = pd.DataFrame(data)
# df.to_parquet('start_end.parquet')

