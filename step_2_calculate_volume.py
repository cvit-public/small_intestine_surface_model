"""
Step 2

Calculate the volume for all small_intestine files
"""

import SimpleITK as sitk
import numpy as np
import pygalmesh
from pathlib import Path
from tqdm import tqdm
import pandas as pd
import argparse  # <- Import argparse

def read_sitk(file_path):
    '''
    Returns the numpy array and spacing for simple itk image path
    '''
    sitk_img = sitk.ReadImage(file_path)
    spacing = sitk_img.GetSpacing()
    img_array = (sitk.GetArrayFromImage(sitk_img)).astype(np.uint8)
    img_array = np.swapaxes(img_array, 0,2)   #Numpy loads the SimpleITK image in z, y x directions.
    return img_array, spacing

def compute_volume(img, voxel_dims):
    """
    Computes Volume for CT Segmentation of largest component in ml
    """
    nonzero_voxel_count = np.count_nonzero(img)
    voxel_volume = np.prod(voxel_dims)
    nonzero_voxel_volume = (nonzero_voxel_count * voxel_volume)/1000
    return nonzero_voxel_volume

def convert_nii_gz_to_stl(scaling_value):
    """
    Convert all nii.gz files in our directory to .stl
    The results are stored in folder `data_1_nii_to_stl`
    """
    # Define the directory
    directory = Path('./individual_masks')

    # List all .nii.gz files in the directory and its subdirectories
    nii_files = list(directory.rglob('*small_bowel.nii.gz'))

    # Create an dict to store results
    data = {
        'name': [],
        'volume': [],
        'scaled_volume': []  # <- New column
    }

    for nifty_file in tqdm(nii_files):
        img_array, spacing = read_sitk(nifty_file)
        nonzero_voxel_volume = compute_volume(img_array, spacing)
        name = nifty_file.parts[1]
        data['name'].append(name)
        data['volume'].append(nonzero_voxel_volume)
        data['scaled_volume'].append(nonzero_voxel_volume * scaling_value)  # Compute scaled volume

    # Save the results to a CSV file
    pd.DataFrame(data).to_csv('volume.csv')


if __name__ == '__main__':
    # Argument parser to get the scaling value from CLI
    parser = argparse.ArgumentParser(description="Scale volume using a given scaling factor.")
    parser.add_argument("scaling_value", type=float, help="The scaling value to multiply with volume.")
    args = parser.parse_args()
    
    convert_nii_gz_to_stl(args.scaling_value)
