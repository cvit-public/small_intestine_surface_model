#!/bin/bash

# Get the directory of the current script
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# Check if the correct number of arguments are provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <alpha> <offset>"
    exit 1
fi

alpha=$1
offset=$2

# Path to the list of cases
CASES_FILE="$SCRIPT_DIR/../case_list.txt"

# Check if the case list file exists
if [ ! -f "$CASES_FILE" ]; then
    echo "Cases file $CASES_FILE does not exist."
    exit 1
fi

# Loop through each case in the case list file
while read -r case; do

    # Specify the directory you want to search
    run_path="$SCRIPT_DIR/../data/${case}/2_run_to_csv/"
    frame="$SCRIPT_DIR/../data/${case}/small_bowel_alpha_shape_${alpha}_${offset}.stl"

    # Check if the case directory exists
    if [ ! -d "$run_path" ]; then
        echo "Directory for case ${case} does not exist."
        continue
    fi

    # Loop over all CSV files in the specified directory (non-recursive)
    find "$run_path" -maxdepth 1 -type f -name "*.csv" | while read -r path; do
      if [ -e "$path" ] && [ -e "$frame" ]; then
          csv_file_name=$(basename "$path" .csv)
          result_file_name="${csv_file_name}.nrb"

          "$SCRIPT_DIR/grow_sm_intest_linux" -v $volume "$path" "$frame" "$result_file_name"

          dest_dir="$SCRIPT_DIR/../data/${case}/3_nurbs"
          mkdir -p "$dest_dir"
          mv "$SCRIPT_DIR/../${result_file_name}" "${dest_dir}/${result_file_name}"
          echo $run_path
      fi
    done

done < "$CASES_FILE"

