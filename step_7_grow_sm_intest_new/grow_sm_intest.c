/*
  PROGRAM TO GROW SMALL INTESTINE USING INITIAL TUBE SURFACE DEFINED BY CSV FILE AND BOUNDING STL SURFACE 
  5/17/23
  Paul Segars
*/

#include <stdio.h>          
#include <stdlib.h>
#include <math.h>
#include <string.h>	

#include "nurbs.h"            /* header file for nurbs */
#include "nurbs.c"            /* NURBS routines */

#define p_degree        3
#define q_degree        3
#define PI        3.141592654

#define Abort(Mesg){fprintf(stderr, "%s\n", Mesg); exit(1);}

float pixel_width, slice_width;

float xoff = 0.0, yoff = 0.0, zoff = 0.0;
int txdim, tydim, tzdim;
int xdim, ydim, zdim;

POINT *tmp_pts;

SURFACE sm_intest, sm_intest_exp;
SURFACE tri_sm_intest;

SURFACE sm_intest_boundary;

SURFACE bounding_models[1000];
BEZIER_MODEL bez_bounding_models[1000];
int NUM_MODELS = 0;

SURFACE surf1, surf2, tmp_surf, vol, vol2;
BEZIER_MODEL bez_model_surf1, bez_model_surf2;

void initialize_structure(SURFACE *nrb_model, int n, int m)
/*------------------------------------------------------------------------------
**  This subroutine initializes the specified NURBS surface
**    m,n:      number of control points in the v and u directions of the surface
**-------------------------------------------------------------------------------
*/
{
  int i, j;

  /*Setup NURB surfaces of the heart*/
  if(nrb_model->net.Pw == NULL)
    nrb_model->net.Pw = cp_matrix(0, n-1, 0 , m-1);

  for(i = 0; i < m; i++)
    for(j = 0; j < n; j++)
      {
      nrb_model->net.Pw[j][i].x = 0.0;
      nrb_model->net.Pw[j][i].y = 0.0;
      nrb_model->net.Pw[j][i].z = 0.0;
      }

  if(nrb_model->orig_Pw == NULL)
    nrb_model->orig_Pw = p_matrix(0, n-1, 0, m-1);

  nrb_model->net.n = n;
  nrb_model->net.m = m;
  if(nrb_model->knu.U == NULL)
    nrb_model->knu.U = vector(0, n + p_degree);
  nrb_model->knu.m = n + p_degree;
  if(nrb_model->knv.U == NULL)
    nrb_model->knv.U = vector(0, m + q_degree);
  nrb_model->knv.m = m + q_degree;

  if(nrb_model->centerline == NULL)
    nrb_model->centerline = p_vector(0, m);

  for(i = 0; i <= 3; i++)
    nrb_model->knu.U[i] = 0.0;
  for(i = nrb_model->net.n; i <= n+3; i++)
    nrb_model->knu.U[i] = 1.0;
  for(i = 4; i <= n-1; i++)
    nrb_model->knu.U[i] = (float)(i-3.0)/ (n-3.0);

  for(i = 0; i <= 3; i++)
    nrb_model->knv.U[i] = 0.0;
  for(i = nrb_model->net.m; i <= m+3; i++)
    nrb_model->knv.U[i] = 1.0;
  for(i = 4; i <= m-1; i++)
    nrb_model->knv.U[i] = (float)(i-3.0)/ (m-3.0);
}

void Read_small_intest_surf(char *filename)
//Reads CSV file that has the control points defining the surface
{
  FILE *fp;
  int i, j;
  int n = 100, m = 900;
  float tx, ty, tz;
  char comma;
  float topx = 0, topy = 0, topz = 0;
  float botx = 0, boty = 0, botz = 0;

  if((fp = fopen(filename, "r")) == NULL)
    Abort("Can not open small intestine pipe file");

  sprintf(sm_intest.name, "small_intestine");
  sm_intest.type = 0;

  /* Setup the surface */
  initialize_structure(&sm_intest, n, m+2);
 
  /*Readin Control Points*/
  for(i = 1; i <= m; i++)
    {
    for(j = 0; j < n; j++)  
      {
      fscanf(fp, "%f%c%f%c%f", &tx, &comma, &ty, &comma, &tz);
	  if(j < n-1)
        fscanf(fp, "%c", &comma);

	  if(i == 1)
	    {
        topx += tx;
        topy += ty;
        topz += tz;
	    }
	  else if(i == m)
	    {
        botx += tx;
        boty += ty;
        botz += tz;
	    }

      sm_intest.net.Pw[j][i].x = tx;
      sm_intest.net.Pw[j][i].y = ty;
      sm_intest.net.Pw[j][i].z = tz;
      sm_intest.net.Pw[j][i].w = 0.0;
      }
    sm_intest.net.Pw[n-1][i].x = sm_intest.net.Pw[0][i].x;
    sm_intest.net.Pw[n-1][i].y = sm_intest.net.Pw[0][i].y;
    sm_intest.net.Pw[n-1][i].z = sm_intest.net.Pw[0][i].z;
    }

  topx /= n;
  topy /= n;
  topz /= n;

  botx /= n;
  boty /= n;
  botz /= n;

  i = 0;
  for(j = 0; j < n; j++)  
    {
    sm_intest.net.Pw[j][i].x = topx;
    sm_intest.net.Pw[j][i].y = topy;
    sm_intest.net.Pw[j][i].z = topz;
    sm_intest.net.Pw[j][i].w = 0.0;
    }

  i = m+1;
  for(j = 0; j < n; j++)  
    {
    sm_intest.net.Pw[j][i].x = botx;
    sm_intest.net.Pw[j][i].y = boty;
    sm_intest.net.Pw[j][i].z = botz;
    sm_intest.net.Pw[j][i].w = 0.0;
    }

  fclose(fp);
}

void Read_small_intest_surf_nrb(char *filename)
//Reads surface from .nrb file
{
  FILE *fp;
  int i, j;
  int n, m;
  float temp, tx, ty, tz;
  char line[200], comma;
  int tmp;
  int num;
  int count1, count2;
  float total_frac;

  if((fp = fopen(filename, "r")) == NULL)
    Abort("Can not open anatomy file");

  tmp = fscanf(fp, "%s", line);
  sprintf(sm_intest.name, "%s", line);

  printf("\nReading %s", line);
  sm_intest.type = 0;

  /* Read in M and N parameters */
  fscanf(fp, "%i", &m); fscanf(fp, "%s", line);
  fscanf(fp, "%i", &n); fscanf(fp, "%s", line);

  /* Setup the surface */
  initialize_structure(&sm_intest, n, m);

  /* Read in U Knot Vector */
  fscanf(fp, "%s", line); fscanf(fp, "%s", line); fscanf(fp, "%s", line);
  for(i = 0; i <= n+p_degree; i++)
    fscanf(fp, "%f", &temp);

  /* Read in V Knot Vector */
  fscanf(fp, "%s", line); fscanf(fp, "%s", line); fscanf(fp, "%s", line);
  for(i = 0; i <= m+q_degree; i++)
    fscanf(fp, "%f", &temp);
  
  /*Readin Control Points*/
  fscanf(fp, "%s", line); fscanf(fp, "%s", line);
  for(i = 0; i < m; i++)
    for(j = 0; j< n; j++)  
      {
      fscanf(fp, "%f%c%f%c%f", &tx, &comma, &ty, &comma, &tz);

      sm_intest.net.Pw[j][i].x = tx;
      sm_intest.net.Pw[j][i].y = ty;
      sm_intest.net.Pw[j][i].z = tz;
      sm_intest.net.Pw[j][i].w = 0.0;
      }
 
  fclose(fp);
}

void Read_bounding_file(SURFACE *nrb_model, char *filename)
//Function to read more than one bounding surface
{
  FILE *fp;
  int i, j, k, l;
  int n, m;
  float temp, tx, ty, tz;
  char line[200], comma;
  int tmp;
  int num;
  int count1, count2;
  float total_frac;

  if((fp = fopen(filename, "r")) == NULL)
    Abort("Can not open anatomy file");

  tmp = fscanf(fp, "%s", line);

  k = 0;
  while(tmp != EOF)
    {
    printf("\nReading %s", line);
    sprintf(nrb_model[k].name, "%s", line);

    if(line[0] == 't' && line[1] == 'm' && line[2] == 'o' && line[3] == 'd')
      {
      nrb_model[k].type = 1;

      fscanf(fp, "%i", &num);
 
      nrb_model[k].net.m = num;
      nrb_model[k].net.n = 3;

      initialize_structure(&nrb_model[k], nrb_model[k].net.n, nrb_model[k].net.m);

      for(i = 0; i < nrb_model[k].net.m; i++)
        {
        for(j = 0; j < nrb_model[k].net.n; j++)
          {
          fscanf(fp, "%f%c%f%c%f", &tx, &comma, &ty, &comma, &tz);

          nrb_model[k].net.Pw[j][i].x = tx;
          nrb_model[k].net.Pw[j][i].y = ty;
          nrb_model[k].net.Pw[j][i].z = tz;
          }
        }
      }    
    else
      {
      nrb_model[k].type = 0;

      /* Read in M and N parameters */
      fscanf(fp, "%i", &m); fscanf(fp, "%s", line);
      fscanf(fp, "%i", &n); fscanf(fp, "%s", line);

      /* Setup the surface */
      initialize_structure(&nrb_model[k], n, m);

      /* Read in U Knot Vector */
      fscanf(fp, "%s", line); fscanf(fp, "%s", line); fscanf(fp, "%s", line);
      for(i = 0; i <= n+p_degree; i++)
        fscanf(fp, "%f", &temp);

      /* Read in V Knot Vector */
      fscanf(fp, "%s", line); fscanf(fp, "%s", line); fscanf(fp, "%s", line);
      for(i = 0; i <= m+q_degree; i++)
        fscanf(fp, "%f", &temp);
  
      /*Readin Control Points*/
      fscanf(fp, "%s", line); fscanf(fp, "%s", line);
      for(i = 0; i < m; i++)
        for(j = 0; j< n; j++)  
          {
          fscanf(fp, "%f%c%f%c%f", &tx, &comma, &ty, &comma, &tz);

          nrb_model[k].net.Pw[j][i].x = tx;
          nrb_model[k].net.Pw[j][i].y = ty;
          nrb_model[k].net.Pw[j][i].z = tz;
          nrb_model[k].net.Pw[j][i].w = 0.0;
          }
      }

    k++;
    tmp = fscanf(fp, "%s", line);
    }

  NUM_MODELS = k;
  fclose(fp);
}


int Round(float f)
/* Rounds the floating point number f to an integer */
{
  float decimal = f - (int)f;

  if(f >= 0)
    {
    if(decimal >=0.5)
      return ( (int)f + 1 );
    else
      return ( (int)f );
    }
  else
    {
    if(decimal <= -0.5)
      return ( (int)f - 1 );
    else
      return ( (int)f );      
    } 
}

int create_bezier_patches2(patch *p, BEZIER_PATCH *patches)
/* Routine decomposes a NURBS surface (initial patch p) into many Bezier patches (patches) */
/* This is done by inserting knots into the NURBS surface until each knot has a multiplicity of 4 */
{
  register int i, j, k, l, l1, k1;
  int umark, vmark;
  int numpt1, numpt2;
  float uval, vval;
  extern statistics_t pstat;
  float minz, maxz, miny, maxy, minx, maxx;
  float pixel_factor;
  pstat.nbezs = 0;
  pixel_factor = 1.0 / (slice_width * 10.0);

  uval = p->kU[p->ordU - 1];
  umark = p->ordU - 1;
  while ((umark > 0) && (p->kU[umark] == uval))
    umark--;
  if (p->kU[umark] < uval)
    umark++;

  vval = p->kV[p->ordV - 1];
  vmark = p->ordV - 1;
  while ((vmark > 0) && (p->kV[vmark] == vval))
    vmark--;
  if (p->kV[vmark] < vval)
    vmark++;

  for (l1=0, l = umark; l <= (p->numU - p->ordU); l += p->ordU,l1++)
    {
    for (k1=0, k = vmark; k <= (p->numV - p->ordV); k += p->ordV,k1++)
      {
      minz = p->points[k][l].z;
      maxz = minz;
      miny = p->points[k][l].y;
      maxy = miny;
      minx = p->points[k][l].x;
      maxx = minx;
      for (j = 0; j < p->ordU; j++)
        for (i = 0; i < p->ordV; i++)
          {
          patches[pstat.nbezs].slice_points[j][i][0] = (p->points[k+i][l+j].x) / (10*pixel_width);
          patches[pstat.nbezs].slice_points[j][i][1] = (p->points[k+i][l+j].y) / (10*pixel_width);
          patches[pstat.nbezs].slice_points[j][i][2] = (p->points[k+i][l+j].z) / (10*slice_width);

          if(p->points[k+i][l+j].z > maxz)
            maxz = p->points[k+i][l+j].z;
          if(p->points[k+i][l+j].z < minz)
            minz = p->points[k+i][l+j].z;
          if(p->points[k+i][l+j].y > maxy)
            maxy = p->points[k+i][l+j].y;
          if(p->points[k+i][l+j].y < miny)
            miny = p->points[k+i][l+j].y;
          if(p->points[k+i][l+j].x > maxx)
            maxx = p->points[k+i][l+j].x;
          if(p->points[k+i][l+j].x < minx)
            minx = p->points[k+i][l+j].x;
          }

       patches[pstat.nbezs].slice_minz = floor(minz*pixel_factor + 0.5);
       patches[pstat.nbezs].slice_maxz = floor(maxz*pixel_factor + 0.5);

       patches[pstat.nbezs].maxx = (maxx) / (10.0*pixel_width);
       patches[pstat.nbezs].minx = (minx) / (10.0*pixel_width);
       patches[pstat.nbezs].maxy = (maxy) / (10.0*pixel_width);
       patches[pstat.nbezs].miny = (miny) / (10.0*pixel_width);
       patches[pstat.nbezs].maxz = (maxz) / (10.0*slice_width);
       patches[pstat.nbezs].minz = (minz) / (10.0*slice_width);

       numpt1 = 0; numpt2 = 0;

       pstat.nbezs++;
       pstat.tot_trimpts1 += numpt1;
       pstat.tot_trimpts2 += numpt2;
      }
    }
  return 0;
}

void SPLINE2BEZ2(SURFACE *nrb_model, BEZIER_MODEL *bez_model)
{
  patch src, dest;
       
  pstat.count = 0;
  pstat.tot_nbezs = 0;
  pstat.tot_trimpts1 = 0;
  pstat.tot_trimpts2 = 0;
           
  setup_initial_patch(&src, nrb_model);
  insert_multiple_knots(&src, &dest);
  refine_patch(&src, &dest);
  create_bezier_patches2(&dest, bez_model->patches);

  free_patch(&dest);
  free_patch(&src);
}

/*----------------------------------------------------------------*/   
/*----------------------------------------------------------------*/   
void Output(SURFACE *nrb_model, FILE *fp)
/*------------------------------------------------------------------
**  This subroutine is used to output a NURBS surface to a file. 
**  This was used to debug the program.
**------------------------------------------------------------------
*/
{
  int i, j;
  float pt[4];

  if(nrb_model->net.n <= 0 || nrb_model->net.m <= 0)
    return;

  if(nrb_model->type == 0)
    {
    fprintf(fp, "%s", nrb_model->name);
    fprintf(fp, "\n%i :M", nrb_model->net.m);
    fprintf(fp, "\n%i :N", nrb_model->net.n);
    fprintf(fp, "\nU Knot Vector");
    for(i = 0; i <= nrb_model->net.n + 3; i++)
      fprintf(fp, "\n%f", nrb_model->knu.U[i]);
    fprintf(fp, "\nV Knot Vector");
    for(i = 0; i <= nrb_model->net.m + 3; i++)
      fprintf(fp, "\n%f", nrb_model->knv.U[i]);
    fprintf(fp, "\nControl Points");
    for(i = 0; i < nrb_model->net.m; i++)
      {
      for(j = 0; j < nrb_model->net.n; j++)
        {
        pt[1] = nrb_model->net.Pw[j][i].x;
        pt[2] = nrb_model->net.Pw[j][i].y;
        pt[3] = nrb_model->net.Pw[j][i].z;
        fprintf(fp, "\n%f,%f,%f", pt[1], pt[2], pt[3]);
        }
      if(i == nrb_model->net.m-1)
        fprintf(fp, "\n");
      }
    }
  else if(nrb_model->type == 1)
    {
    fprintf(fp, "%s", nrb_model->name);
    fprintf(fp, "\n%i", nrb_model->net.m);

    for(i = 0; i < nrb_model->net.m; i++)
      {
      fprintf(fp, "\n");
      for(j = 0; j < 3; j++)
         fprintf(fp, "%f,%f,%f ", nrb_model->net.Pw[j][i].x, nrb_model->net.Pw[j][i].y, nrb_model->net.Pw[j][i].z);
      }
    fprintf(fp, "\n");
    }
}

void OutputSurfRhino(SURFACE surf, char *filename)
{
  FILE *fp;

  if((fp = fopen(filename, "a")) == NULL)
    Abort("Can not open output for Rhino file");

  Output(&surf, fp);
  fclose(fp);
}

/*----------------------------------Render Routines-------------------------------------*/
void Write(int intensity, int x, int y, float *out)
{
 if(x < 0)
   x = 0;
 if(x >= txdim)
   x = txdim -1;
 if(y < 0)
   y = 0;
 if(y >= tydim)
   y = tydim -1;
 
 if ( (0<=(y)) && ((y)<tydim) && (0<=(x)) && ((x)<txdim))
   out[(y)*txdim + (x)] = intensity;
}

void G_line(int x,int y,int x2,int y2, int intensity, float *out)
{
 int dx,dy,long_d,short_d;
 int d,add_dh,add_dl;
 register int inc_xh,inc_yh,inc_xl,inc_yl;
 register int i;

 /* Fix y value for screen */
 y =  y;
 y2 = y2;

 dx=x2-x; dy=y2-y;                          /* ranges */

 if(dx<0){dx = -dx; inc_xh = -1; inc_xl = -1;}    /* making sure dx and dy >0 */
 else    {        inc_xh = 1;  inc_xl = 1; }    /* adjusting increments */
 if(dy<0){dy = -dy; inc_yh = -1; inc_yl = -1;}
 else    {        inc_yh = 1;  inc_yl = 1; }
          
 if(dx>dy){long_d=dx; short_d=dy; inc_yl=0;} /* long range,&making sure either */
 else     {long_d=dy; short_d=dx; inc_xl=0;} /* x or y is changed in L case */

 d=2*short_d-long_d;                        /* initial value of d */
 add_dl=2*short_d;                          /* d adjustment for H case */
 add_dh=2*short_d-2*long_d;                 /* d adjustment for L case */

 for(i=0;i<=long_d;i++)                     /* for all points in longer range */
 {
  Write(intensity,x,y, out);                        /* rendering */

  if(d>=0){x+=inc_xh; y+=inc_yh; d+=add_dh;}/* previous point was H type*/
  else    {x+=inc_xl; y+=inc_yl; d+=add_dl;}/* previous point was L type*/
 }
}

int Check_Y_Boundary(int x, int y, int threshold, float *out)
{
  int bound1=0, bound2=0;
  int i;
  unsigned long int index;

  i = y;
  while(!bound1 && i < tydim)
    {
    index = x + i * txdim;
    if( (int)out[index] == threshold)
      bound1 = 1;
    i++;
    }
 
  i = y;
  while(!bound2 && i >= 0)
    {
    index = x+ i * txdim;     
    if( (int)out[index] == threshold)
      bound2 = 1;
    i--;
    }

  return(bound1&&bound2);
}

void Fill(int intensity, float *out)
{   
   int x, y;
   unsigned long index;
   int diff;
   int *edge;
   int edge_counter;
   int flag = 0;
   int i;
  
   edge = (int *)malloc((unsigned)sizeof(int) * txdim); 
   for(i = 0; i < txdim; i++)
     edge[i] = -1;

   for(y = 0; y < tydim; y++) {  
     edge_counter = 0;
     for(x = 0; x < txdim; x++) {
       index = x+ y * txdim;     
       diff = (int)out[index];
       if( diff == intensity) 
         {
         edge[edge_counter] = x;
         edge_counter++;
         if(edge_counter == txdim)
           {
           printf("\nERROR: Edge_counter = %i", edge_counter);
           exit(1);
           }
         }
     }

     while(edge_counter!=0)
       {
       flag = 0;
       if(edge_counter-2 >=0)
         {
         if(edge[edge_counter-1] != -1 && edge[edge_counter-2] != -1)
           {
           if(edge[edge_counter-1] - edge[edge_counter-2] > 1)
             flag = Check_Y_Boundary( (edge[edge_counter-2]+edge[edge_counter-1])/2, y,intensity,out);

           if(flag)
             {           
             G_line(edge[edge_counter-2], y, edge[edge_counter-1], y, intensity, out);
             flag = 0;
             }
           }
         }
       edge_counter--;
       }
   }

  free(edge);
}


void lp_intersection(double *p0, double *p1, int z, int *result, int *flag)
{
  double t;

  *flag = 0;
  /*return with flag = 0 means no intersection*/
  /*return with flag = 1 means there is an intersection*/
  /*return with flag = 2 means line is on the plane*/

  result[0] = 0; result[1] = 0; result[2] = 0;

  if(p1[2]!= p0[2])
    {
    t = ( (double)z - p0[2]) / (p1[2] - p0[2]);
    if(t >= 0 && t <= 1)
      {
      *flag = 1;
      result[0] = floor( p0[0] + t * (p1[0] - p0[0]) +0.5);
      result[1] = floor( p0[1] + t * (p1[1] - p0[1]) +0.5);
      result[2] = z;    
      }
    }
  else if(p0[2] == z)
    *flag = 2;
}

int Test_patch_z(double patch[4][4][3], int z)
{
  int i = 0, j;
  double A, B, C, D;
  double denom;
  double t_error;
  double max_error;

  Plane_eqn(patch[0][0], patch[0][3], patch[3][0], &A, &B, &C, &D); 
  denom = sqrt(A * A + B * B + C * C);
  if(denom == 0.0)
    {
    Plane_eqn(patch[0][3], patch[3][0], patch[3][3], &A, &B, &C, &D); 
    denom = sqrt(A * A + B * B + C * C);
    }
  if(denom == 0.0)
    {
    Plane_eqn(patch[0][0], patch[0][3], patch[3][3], &A, &B, &C, &D); 
    denom = sqrt(A * A + B * B + C * C);
    }
  if(denom == 0.0)
    {
    Plane_eqn(patch[0][0], patch[3][0], patch[3][3], &A, &B, &C, &D); 
    denom = sqrt(A * A + B * B + C * C);
    }
  if(denom == 0.0)
    {
    i = 0;
    while(denom == 0.0 && i <= 2)
      {
      j = 0;
      while(denom == 0.0 && j <= 2)
        {
        Plane_eqn(patch[i][j], patch[i][j+1], patch[i+1][j], &A, &B, &C, &D);
        j++;
        }
      i++;
      }  
    denom = sqrt(A * A + B * B + C * C);
    }
  if(denom == 0.0)
    {
    while(denom == 0.0 && i <= 2)
      {
      j = 0;
      while(denom == 0.0 && j <= 2)
        {
        Plane_eqn(patch[i][j], patch[i][j+1], patch[i+1][j+1], &A, &B, &C, &D);
        j++;
        }
      i++;
      }  
    denom = sqrt(A * A + B * B + C * C);
    }
  if(denom == 0.0)
    return 1;

  max_error = 0.0;
  for(i = 0; i < 4; i++)
    for(j = 0; j < 4; j++)
      {   
      if(denom != 0)
        t_error = fabs( (A * patch[i][j][0] + B * patch[i][j][1] + C * patch[i][j][2] + D) / denom);
      else
        t_error = 0.0;
      if(t_error > max_error)
        max_error = t_error;
      }

  if(max_error < 0.001 )
    return 1;
  else
    return 0;
  }

void Render_patch(double patch[4][4][3], int intensity, float *out, int z)
{
  int flag1, flag2, flag3;
  int int1[3], int2[3], int3[3];
                         
  /*Render 1st triangle*/
  lp_intersection(patch[3][0], patch[3][3], z, int1, &flag1);
  lp_intersection(patch[0][0], patch[3][3], z, int2, &flag2);
  lp_intersection(patch[3][0], patch[0][0], z, int3, &flag3);
  

  if(flag1 == 1 && flag2 == 1)
    G_line(int1[0], int1[1], int2[0], int2[1], intensity, out);
  else if(flag1 == 1 && flag3 == 1)
    G_line(int1[0], int1[1], int3[0], int3[1], intensity, out);
  else if(flag3 == 1 && flag2 == 1)
    G_line(int3[0], int3[1], int2[0], int2[1], intensity, out);

  /*Render 2nd triangle*/
  lp_intersection(patch[0][0], patch[3][3], z, int1, &flag1);
  lp_intersection(patch[0][3], patch[3][3], z, int2, &flag2);
  lp_intersection(patch[0][0], patch[0][3], z, int3, &flag3);

  if(flag1 == 1 && flag2 == 1)
    G_line(int1[0], int1[1], int2[0], int2[1], intensity, out);
  else if(flag1 == 1 && flag3 == 1)
    G_line(int1[0], int1[1], int3[0], int3[1], intensity, out);
  else if(flag3 == 1 && flag2 == 1)
    G_line(int3[0], int3[1], int2[0], int2[1], intensity, out); 
}


int Test_extents_z(double patch[4][4][3], int z)
{
  int i, j;
  double minz, maxz;
  double z_test;

  z_test = z;

  minz = patch[0][0][2];
  maxz = minz;

  for(i = 0; i < 4; i++)
    for(j = 0; j < 4; j++)
      {
       if(patch[i][j][2] < minz)
         minz = patch[i][j][2];
       if(patch[i][j][2] > maxz)
         maxz = patch[i][j][2];
      }

  if(z_test >= minz && z_test <= maxz)
    return 1;
  else
   return 0;
}

void Intersect_bez_z(double patch[4][4][3], float *out, int intensity, int z)
{
  double ul_patch[4][4][3], ur_patch[4][4][3], dl_patch[4][4][3], dr_patch[4][4][3];

  if( Test_patch_z(patch, z) )
    Render_patch(patch, intensity, out, z);
  else
    {
    Subdivide_patch(patch, ul_patch, ur_patch, dl_patch, dr_patch);
    if(Test_extents_z(ul_patch, z))
      Intersect_bez_z(ul_patch, out, intensity, z);
    if(Test_extents_z(ur_patch, z))
      Intersect_bez_z(ur_patch, out, intensity, z);  
    if(Test_extents_z(dl_patch, z))
      Intersect_bez_z(dl_patch, out, intensity, z);  
    if(Test_extents_z(dr_patch, z))
      Intersect_bez_z(dr_patch, out, intensity, z);  
    }
}

void Render_Bezier(BEZIER_MODEL *bez_model, float *out, int intensity, int z)
{
  int i;

  for(i = 0; i < bez_model->num_patches; i++)
    {
    if(z >= bez_model->patches[i].slice_minz && z <= bez_model->patches[i].slice_maxz)
      Intersect_bez_z(bez_model->patches[i].slice_points, out, intensity, z);
    }
}

void Render_triangle(TRIANGLE tri, int intensity, float *out, int z)
{
  int flag1, flag2, flag3, j;
  int int1[3], int2[3], int3[3];
  double v[3][3];

  for(j = 0; j < 3; j++)
    {
    v[j][0] = (tri.vertex[j].x) / (pixel_width);
    v[j][1] = (tri.vertex[j].y) / (pixel_width);
    v[j][2] = (tri.vertex[j].z) / (slice_width);
    }

  /*Render triangle*/
  lp_intersection(v[0], v[2], z, int1, &flag1);
  lp_intersection(v[1], v[2], z, int2, &flag2);
  lp_intersection(v[0], v[1], z, int3, &flag3);

  if(flag1 == 1 && flag2 == 1)
    G_line(int1[0], int1[1], int2[0], int2[1], intensity, out);
  else if(flag1 == 1 && flag3 == 1)
    G_line(int1[0], int1[1], int3[0], int3[1], intensity, out);
  else if(flag3 == 1 && flag2 == 1)
    G_line(int3[0], int3[1], int2[0], int2[1], intensity, out);
}


#define EPSILON 0.000001
void CROSS(double dest[3], double v1[3], double v2[3])
{ 
          dest[0]=v1[1]*v2[2]-v1[2]*v2[1]; 
          dest[1]=v1[2]*v2[0]-v1[0]*v2[2]; 
          dest[2]=v1[0]*v2[1]-v1[1]*v2[0];
}

#define DOT(v1,v2) (v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2])
void SUB(double dest[3], double v1[3], double v2[3])
{
          dest[0]=v1[0]-v2[0]; 
          dest[1]=v1[1]-v2[1]; 
          dest[2]=v1[2]-v2[2]; 
}

int intersect_triangle(double orig[3], double dir[3],
                       TRIANGLE T,
                       double *t, double *u, double *v)
{
   double edge1[3], edge2[3], tvec[3], pvec[3], qvec[3];
   double det,inv_det;
   double vert0[3], vert1[3], vert2[3];

   vert0[0] = T.vertex[0].x; vert0[1] = T.vertex[0].y; vert0[2] = T.vertex[0].z;
   vert1[0] = T.vertex[1].x; vert1[1] = T.vertex[1].y; vert1[2] = T.vertex[1].z;
   vert2[0] = T.vertex[2].x; vert2[1] = T.vertex[2].y; vert2[2] = T.vertex[2].z;

   /* find vectors for two edges sharing vert0 */
   SUB(edge1, vert1, vert0);
   SUB(edge2, vert2, vert0);

   /* begin calculating determinant - also used to calculate U parameter */
   CROSS(pvec, dir, edge2);

   /* if determinant is near zero, ray lies in plane of triangle */
   det = DOT(edge1, pvec);

   if (det > -EPSILON && det < EPSILON)
     return 0;
   inv_det = 1.0 / det;

   /* calculate distance from vert0 to ray origin */
   SUB(tvec, orig, vert0);

   /* calculate U parameter and test bounds */
   *u = DOT(tvec, pvec) * inv_det;
   if (*u < 0.0 || *u > 1.0)
     return 0;

   /* prepare to test V parameter */
   CROSS(qvec, tvec, edge1);

   /* calculate V parameter and test bounds */
   *v = DOT(dir, qvec) * inv_det;
   if (*v < 0.0 || *u + *v > 1.0)
     return 0;

   /* calculate t, ray intersects triangle */
   *t = DOT(edge2, qvec) * inv_det;

  return 1;
}


void Find_Intersections_tri(TRI_MODEL tri_model, int organ_id, float line_origin[3],float line_vector[3], XP_ARRAY *int_points)
{
  int i;
  double u, v, t;
  double orig[3], dir[3];
  int count, count2, flag;

  orig[0] = line_origin[0];
  orig[1] = line_origin[1];
  orig[2] = line_origin[2];

  dir[0] = line_vector[0];
  dir[1] = line_vector[1];
  dir[2] = line_vector[2];

  for(i = 0; i < tri_model.num_tris; i++)
    {
    if(intersect_triangle(orig, dir, tri_model.tris[i], &t, &u, &v))
        {
        if(t > 0 && int_points->length == 0)
          {
          int_points->length = 1;
          int_points->xp[0].x = t;
          int_points->xp[0].organ_id = organ_id;
          }
        else if(t > 0)
          {
          count = 0;
          flag = 1;
          while(count < int_points->length && flag)
            if(t > int_points->xp[count].x)
              count++;
            else
              flag = 0;

		  for(count2 = int_points->length; count2 > count; count2--)
            {
            int_points->xp[count2].x = int_points->xp[count2-1].x;
            int_points->xp[count2].organ_id = int_points->xp[count2-1].organ_id;
            }

          int_points->xp[count].x = t;
          int_points->xp[count].organ_id = organ_id;
          int_points->length++;
          }
        }
    }

}

int Check_Y_Boundary2(TRI_MODEL slice_tmodel, int x, int y, int z, int intensity)
{
  int j, i, flag;
  float line_origin[3], line_vector[3];
  XP_ARRAY int_points;

  line_origin[0] = (x)*pixel_width;
  line_origin[1] = (y)*pixel_width;
  line_origin[2] = (z)*slice_width;

  line_vector[0] = 1;
  line_vector[1] = 0;
  line_vector[2] = 0;

  int_points.length = 0;
  Find_Intersections_tri(slice_tmodel, intensity, line_origin, line_vector, &int_points);

  if(int_points.length % 2 == 0)
    return 0;
  else
    return 1;
}

void Fill_tri(TRI_MODEL slice_tmodel, int intensity, float *out, int z)
{
   int x, y;
   unsigned long index;
   int diff;
   int edge[2000];
   int edge_counter;
   int counter = 0;
   int flag = 0;
   int i;

   for(i = 0; i < 2000; i++)
     edge[i] = -1;

   for(y = 0; y < tydim; y++)
     {
     edge_counter = 0;
     for(x = 0; x < txdim; x++)
       {
       index = x+ y*txdim;
       diff = (int)out[index];
       if( diff == intensity)
         {
         edge[edge_counter] = x;
         edge_counter++;
         }
       }

     while(edge_counter!=0)
       {
       flag = 0;
       if(edge_counter-2 >=0)
         {
         if(edge[edge_counter-1] != -1 && edge[edge_counter-2] != -1)
           {
           if(edge[edge_counter-1] - edge[edge_counter-2] > 1)
             flag = Check_Y_Boundary2(slice_tmodel, (edge[edge_counter-2]+edge[edge_counter-1])/2, y, z, intensity);
           if(flag)
             {
             G_line(edge[edge_counter-2], y, edge[edge_counter-1], y, intensity, out);
             flag = 0;
             }
           }
         }
       edge_counter--;
       }
     }
}

int Test_extents_tri_z(TRIANGLE tri, int z)
{
  int j;
  double minz, maxz;

  minz = tri.vertex[0].z;
  maxz = minz;

  for(j = 1; j < 3; j++)
    {
     if(tri.vertex[j].z < minz)
       minz = tri.vertex[j].z;
     if(tri.vertex[j].z > maxz)
       maxz = tri.vertex[j].z;
    }

  minz = (minz) / (slice_width);
  maxz = (maxz) / (slice_width);

  if(z >= minz-1 && z <= maxz+1)
    return 1;
  else
   return 0;
}

TRI_MODEL slice_tmodel;
float Render_Tri_Model(MOTION_VECTORS *mv, SURFACE nrb_model, float intensity)
{
  int i, j, k, l, num;
  unsigned index1, index2;
  float volume = 0.0;
  float *slice;
  TRIANGLE tmp_tri;

  pixel_width = mv->pixel_width;
  slice_width = mv->slice_width;
  txdim = mv->XDIM;
  tydim = mv->YDIM;
  tzdim = mv->ZDIM;

  slice = vector(0, txdim*tydim);

  for(i = 0; i < nrb_model.net.m; i++)
    for(j = 0; j < nrb_model.net.n; j++)
      {
      nrb_model.net.Pw[j][i].x -= mv->xoff;
      nrb_model.net.Pw[j][i].y -= mv->yoff;
      nrb_model.net.Pw[j][i].z -= mv->zoff;
      }

  for(j = 0; j < tzdim; j++) 
    {
    /*Initialize slice image */
     for(i = 0; i < txdim*tydim; i++)
       slice[i] = -1.0;

    num = 0;
    slice_tmodel.num_tris = num;
    for(k = 0; k < nrb_model.net.m; k++)
      {
      for(l = 0; l < 3; l++)
        {
        tmp_tri.vertex[l].x = nrb_model.net.Pw[l][k].x;
        tmp_tri.vertex[l].y = nrb_model.net.Pw[l][k].y;
        tmp_tri.vertex[l].z = nrb_model.net.Pw[l][k].z;
        }

      if(Test_extents_tri_z(tmp_tri, j))
        {
        slice_tmodel.tris[num].vertex[0].x = nrb_model.net.Pw[0][k].x;
        slice_tmodel.tris[num].vertex[0].y = nrb_model.net.Pw[0][k].y;
        slice_tmodel.tris[num].vertex[0].z = nrb_model.net.Pw[0][k].z;

        slice_tmodel.tris[num].vertex[1].x = nrb_model.net.Pw[1][k].x;
        slice_tmodel.tris[num].vertex[1].y = nrb_model.net.Pw[1][k].y;
        slice_tmodel.tris[num].vertex[1].z = nrb_model.net.Pw[1][k].z;

        slice_tmodel.tris[num].vertex[2].x = nrb_model.net.Pw[2][k].x;
        slice_tmodel.tris[num].vertex[2].y = nrb_model.net.Pw[2][k].y;
        slice_tmodel.tris[num].vertex[2].z = nrb_model.net.Pw[2][k].z;

        num++;
        slice_tmodel.num_tris = num;
        }
      }
      if(slice_tmodel.num_tris > 0)
        {
        for(i = 0; i < slice_tmodel.num_tris; i++)
          Render_triangle(slice_tmodel.tris[i], intensity, slice, j);
        Fill_tri(slice_tmodel, intensity, slice, j);
        }

	for(k = 0; k < txdim; k++)
      for(l = 0; l < tydim; l++)
        {
        index1 = k + l * txdim + j * txdim * tydim;
        index2 = k + l * txdim;

        if(slice[index2] != -1)
		  {
          //printf("\n%i %i %i = %f", k, l, j, slice[index2]);
          mv->vectors[index1].im1 = slice[index2];
          volume += (mv->pixel_width*mv->pixel_width*mv->slice_width);
		  }
        }
    }

  volume /= 1000; //Convert to mls

  free_vector(slice, 0, txdim*tydim);

  //Reset surfaces
  for(i = 0; i < nrb_model.net.m; i++)
    for(j = 0; j < nrb_model.net.n; j++)
      {
      nrb_model.net.Pw[j][i].x += mv->xoff;
      nrb_model.net.Pw[j][i].y += mv->yoff;
      nrb_model.net.Pw[j][i].z += mv->zoff;
      }
  return volume;
}

/*----------------------------------------------------------------*/
void Calc_extents(SURFACE *nrb_model)
/*----------------------------------------------------------------*/
/*------------------------------------------------------------------
**  This subroutine is used to set the maximum and minimum dimensions
**  of a given NURBS model.  These dimensions are used to pre-render
**  the NURBS surface into its own image before combining the image
**  into the final output image.
**------------------------------------------------------------------
*/
{
  int i, j;
  float tx, ty, tz;
  float min_x, max_x, min_y, max_y, max_z, min_z;

  /*Initialize the maximum and minimum to the first control point*/
  max_z = nrb_model->net.Pw[0][0].z;
  min_z = nrb_model->net.Pw[0][0].z;

  max_y = nrb_model->net.Pw[0][0].y;
  min_y = nrb_model->net.Pw[0][0].y;

  max_x = nrb_model->net.Pw[0][0].x;
  min_x = nrb_model->net.Pw[0][0].x;

  /*Cycle through the model's control points*/
  for(i = 0; i < nrb_model->net.m; i++)  {
    for(j = 0; j< nrb_model->net.n; j++)  {
      tz = nrb_model->net.Pw[j][i].z;
      ty = nrb_model->net.Pw[j][i].y;
      tx = nrb_model->net.Pw[j][i].x;


      /*Find the maximum*/
      if(tz > max_z)
        max_z = tz;

      /*Find the minimum*/
      if(tz < min_z)
        min_z = tz;

      /*Find the maximum*/
      if(ty > max_y)
        max_y = ty;

      /*Find the minimum*/
      if(ty < min_y)
        min_y = ty;

      /*Find the maximum*/
      if(tx > max_x)
        max_x = tx;

      /*Find the minimum*/
      if(tx < min_x)
        min_x = tx;
    }
  }

  nrb_model->max_z = max_z;
  nrb_model->min_z = min_z;

  nrb_model->max_y = max_y;
  nrb_model->min_y = min_y;

  nrb_model->max_x = max_x;
  nrb_model->min_x = min_x;
}
/*----------------------------------------------------------------*/


void RenderInVectors2(SURFACE nrb_model, BEZIER_MODEL bez_model, MOTION_VECTORS *mv, int value)
{
  int i, j, k;
  int a, b, c;
  unsigned int index;
  float *slice;
  float intensity = 1.0;
  int xdim, ydim, zdim;
  float p_width, s_width;
  float offx, offy, offz;
  float subvxl;
  int x, y, z;

  pixel_width = mv->pixel_width/10.0;
  slice_width = mv->slice_width/10.0;
  txdim = mv->XDIM;
  tydim = mv->YDIM;
  tzdim = mv->ZDIM;

  slice = vector(0, txdim*tydim);

  for(i = 0; i < nrb_model.net.m; i++)
    for(j = 0; j < nrb_model.net.n; j++)
      {
      nrb_model.net.Pw[j][i].x -= mv->xoff;
      nrb_model.net.Pw[j][i].y -= mv->yoff;
      nrb_model.net.Pw[j][i].z -= mv->zoff;
      }
  Calc_extents(&nrb_model);
  SETUP_BEZIER_MODEL(nrb_model, &bez_model);
  SPLINE2BEZ2(&nrb_model, &bez_model);

  for(j = 0; j < tzdim; j++)
    {
    for(index = 0; index < txdim*tydim; index++)
      slice[index] = -1;

    intensity = 1;
    Render_Bezier(&bez_model, slice, intensity, j);
    Fill(intensity, slice);

    for(x = 0; x < txdim; x++)
      for(y = 0; y < tydim; y++)
        {
        if(slice[x + y*txdim] == 1)
          mv->vectors[x + y*txdim + j*txdim*tydim].im1 = value;
        }
    }

  free_vector(slice, 0, txdim*tydim);

  //Reset surfaces
  for(i = 0; i < nrb_model.net.m; i++)
    for(j = 0; j < nrb_model.net.n; j++)
      {
      nrb_model.net.Pw[j][i].x += mv->xoff;
      nrb_model.net.Pw[j][i].y += mv->yoff;
      nrb_model.net.Pw[j][i].z += mv->zoff;
      }
  Calc_extents(&nrb_model);
}
/*----------------------End Render Routines-----------------------------*/

void Init_NURBS_Surface2(SURFACE *nrb_model, int n, int m)
/*------------------------------------------------------------------------------
**  This subroutine initializes the specified NURBS surface
**    m,n:      number of control points in the v and u directions of the surface
**-------------------------------------------------------------------------------
*/
{
  int i, j;

  nrb_model->net.n = n;
  nrb_model->net.m = m;

  nrb_model->knu.m = n + 3;
  nrb_model->knv.m = m + 3;

  for(i = 0; i < n; i++)
    for(j = 0; j < m; j++)
     {          
      nrb_model->net.Pw[i][j].x = 0.0;
      nrb_model->net.Pw[i][j].y = 0.0;
      nrb_model->net.Pw[i][j].z = 0.0;
      nrb_model->net.Pw[i][j].w = 0.0;
    }

  for(i = 0; i <= 3; i++)
    nrb_model->knu.U[i] = 0.0;
  for(i = nrb_model->net.n; i <= n+3; i++)
    nrb_model->knu.U[i] = 1.0;
  for(i = 4; i <= n-1; i++)
    nrb_model->knu.U[i] = (float)(i-3.0)/ (n-3.0);

  for(i = 0; i <= 3; i++)
    nrb_model->knv.U[i] = 0.0;
  for(i = nrb_model->net.m; i <= m+3; i++)
    nrb_model->knv.U[i] = 1.0;
  for(i = 4; i <= m-1; i++)
    nrb_model->knv.U[i] = (float)(i-3.0)/ (m-3.0);
}

/*----------------------------------------------------------------*/
/*----------------------------------------------------------------*/
float distance(float x1, float y1, float z1, float x2, float y2, float z2)
/*----------------------------------------------------------------------
**  This subroutine is used to compute the distance between two points.
**----------------------------------------------------------------------
*/
{
float result;

x1 = x1 - x2;
y1 = y1 - y2;
z1 = z1 - z2;

result = sqrt(x1*x1 + y1*y1 + z1*z1);
return result;
}

void FindCenterline(SURFACE *surf)
{
  int i, j;

  for(i = 0; i < surf->net.m; i++)
    {
	surf->centerline[i].x = 0.0;
	surf->centerline[i].y = 0.0;
	surf->centerline[i].z = 0.0;

	for(j = 0; j < surf->net.n-2; j++)
	  {
	  if(j != 1)
	    {
	    surf->centerline[i].x += surf->net.Pw[j][i].x; 
	    surf->centerline[i].y += surf->net.Pw[j][i].y; 
	    surf->centerline[i].z += surf->net.Pw[j][i].z; 
	    }
	  }
	surf->centerline[i].x /= (surf->net.n-3.0);
	surf->centerline[i].y /= (surf->net.n-3.0);
	surf->centerline[i].z /= (surf->net.n-3.0);
    }
}

float FindLength(SURFACE surf)
{
  int i;
  float dist;

  dist = 0.0;
  for(i = 1; i < surf.net.m; i++)
    dist += distance(surf.centerline[i-1].x, surf.centerline[i-1].y, surf.centerline[i-1].z,
                     surf.centerline[i].x, surf.centerline[i].y, surf.centerline[i].z);
  return dist;
}

void FindRadiusStats(SURFACE surf, float *ave, float *min, float *max)
{
  int i, j;
  float radius;
  unsigned long int count;

  *ave = 0.0;
  *min = 100000;
  *max = -100000;
  count = 0;
  for(i = 1; i < surf.net.m-1; i++)
    {
	for(j = 0; j < surf.net.n-2; j++)
	  {
	  if(j != 1)
	    {
        radius = distance(surf.centerline[i].x, surf.centerline[i].y, surf.centerline[i].z,
                          surf.net.Pw[j][i].x, surf.net.Pw[j][i].y, surf.net.Pw[j][i].z);
        *ave += radius;
		count++;

		if(radius < *min)
		  *min = radius;
		if(radius > *max)
		  *max = radius;
	    }
	  }   
    }

  if(count > 0)
    *ave /= count;
}

void generate_structure(SURFACE *surf, SURFACE surf_init, POINT *centerline, float p)
{
  int i, j;

  for(i = 0; i < surf->net.m; i++)
    {
    for(j = 0; j < surf->net.n; j++)
      {
      surf->net.Pw[j][i].x = centerline[i].x + p*(surf_init.net.Pw[j][i].x - centerline[i].x);
      surf->net.Pw[j][i].y = centerline[i].y + p*(surf_init.net.Pw[j][i].y - centerline[i].y);
      surf->net.Pw[j][i].z = centerline[i].z + p*(surf_init.net.Pw[j][i].z - centerline[i].z);
      }
    }
}


void SaveImage(char *filename, int xdim, int ydim, int zdim, float *image_pixel)
{
  int   n, TotalPix;
  FILE  *fp_out;

  TotalPix = xdim*ydim*zdim;   /* total number of pixels in 3D image */
  if ((fp_out = fopen(filename, "wb")) == NULL)
    {
    printf("\nCannot open output file");
    exit(1);
    }

  printf("\nDims %i %i %i", xdim, ydim, zdim);

  n = fwrite (image_pixel, sizeof(float), TotalPix, fp_out);
  if (n != TotalPix) {
    printf("Error : fwrite return %d\n",n);
    printf("\nFailure writing pixels to output image\n");
    exit(1);
  }
  fclose(fp_out);
}

void SetOrigPoints(SURFACE *nrb_model)
{
  int i, j;

  for(i = 0; i < nrb_model->net.m; i++)
    for(j = 0; j < nrb_model->net.n; j++)
      {
      nrb_model->orig_Pw[j][i].x = nrb_model->net.Pw[j][i].x;
      nrb_model->orig_Pw[j][i].y = nrb_model->net.Pw[j][i].y;
      nrb_model->orig_Pw[j][i].z = nrb_model->net.Pw[j][i].z;
      }
}

void SetSurfaceToOrigPoints(SURFACE *surf, SURFACE *surf2)
{
  int i, j;

  for(i = 0; i < surf->net.m; i++)
    for(j = 0; j < surf->net.n; j++)
      {
      surf->net.Pw[j][i].x = surf2->orig_Pw[j][i].x;
      surf->net.Pw[j][i].y = surf2->orig_Pw[j][i].y;
      surf->net.Pw[j][i].z = surf2->orig_Pw[j][i].z;
      }
}


void SetVectorsSurfNonRigid(SURFACE surf, int ID, MOTION_VECTORS *mv, int ave_flag, int factor, int ctrl_flag)
{
  int u, v, x, y, z;
  float uvar, vvar;
  POINT C, C2, C3;
  float dx, dy, dz;
  unsigned long index;
  int u_step, v_step;
  float radius, length;
  int i, j, k;
  int a, b, c;

  initialize_structure(&vol, surf.net.n, surf.net.m);
  CopySurface(&vol, &surf);
  SetSurfaceToOrigPoints(&vol, &surf);

  initialize_structure(&vol2, surf.net.n, surf.net.m);
  CopySurface(&vol2, &surf);
  SetSurfaceToOrigPoints(&vol2, &surf);

  FindCenterline(&surf);
  length = FindLength(surf);
  v_step = floor(factor*length / mv->pixel_width + 0.5);
  if(v_step < 2)
    v_step = 5;

  radius = distance(surf.net.Pw[0][surf.net.m/2].x, surf.net.Pw[0][surf.net.m/2].y, surf.net.Pw[0][surf.net.m/2].z,
                   surf.centerline[surf.net.m/2].x, surf.centerline[surf.net.m/2].y, surf.centerline[surf.net.m/2].z);
  u_step = floor(2*factor*3.14*radius / mv->pixel_width);
  if(u_step == 0)
    u_step = 1;

  if(ctrl_flag)
  {
  for(i = 0; i < surf.net.m; i++)
    for(j = 0; j < surf.net.n; j++)
      {
      dx = surf.net.Pw[j][i].x - vol.net.Pw[j][i].x;
      dy = surf.net.Pw[j][i].y - vol.net.Pw[j][i].y;
      dz = surf.net.Pw[j][i].z - vol.net.Pw[j][i].z;

      x = floor((vol.net.Pw[j][i].x - mv->xoff) / mv->pixel_width + 0.5);
      y = floor((vol.net.Pw[j][i].y - mv->yoff) / mv->pixel_width + 0.5);
      z = floor((vol.net.Pw[j][i].z -mv->zoff) / mv->slice_width + 0.5);

      for(c = z-1; c <= z+1; c++)
      for(b = y-1; b <= y+1; b++)
      for(a = x-1; a <= x+1; a++)
        {
        if(a >= 0 && a < mv->XDIM && b >= 0 && b < mv->YDIM && c >= 0 && c < mv->ZDIM)
          {
          index = a + b*mv->XDIM + c*mv->XDIM*mv->YDIM;

          if(ave_flag)
            {
            mv->vectors[index].x += dx;
            mv->vectors[index].y += dy;
            mv->vectors[index].z += dz;
            mv->vectors[index].count++;
            mv->vectors[index].im1 = ID;
            }
          else
            {
            mv->vectors[index].x = dx;
            mv->vectors[index].y = dy;
            mv->vectors[index].z = dz;
            mv->vectors[index].count = 1;
            mv->vectors[index].im1 = ID;
            }
          }
        }
      }
  }

  for(v = 0; v <= v_step; v++)
    {
    vvar = (float) v / v_step;
    for(u = 0; u <= u_step; u++)
      {
      uvar = (float) u / u_step;

      C = SurfacePoint(vol.net.n-1, p_degree, vol.knu, vol.net.m-1, q_degree,
                       vol.knv, vol.net.Pw, uvar, vvar);
      C2 = SurfacePoint(surf.net.n-1, p_degree, surf.knu, surf.net.m-1, q_degree,
                        surf.knv, surf.net.Pw, uvar, vvar);

      dx = C2.x - C.x;
      dy = C2.y - C.y;
      dz = C2.z - C.z;

      x = floor((C.x - mv->xoff) / mv->pixel_width + 0.5);
      y = floor((C.y - mv->yoff) / mv->pixel_width + 0.5);
      z = floor((C.z -mv->zoff) / mv->slice_width + 0.5);

      if(x >= 0 && x < mv->XDIM && y >= 0 && y < mv->YDIM && z >= 0 && z < mv->ZDIM)
        {
        index = x + y*mv->XDIM + z*mv->XDIM*mv->YDIM;
        if(ave_flag)
          {
          mv->vectors[index].x += dx;
          mv->vectors[index].y += dy;
          mv->vectors[index].z += dz;
          mv->vectors[index].count++;
          mv->vectors[index].im1 = ID;
          }
        else
          {
          mv->vectors[index].x = dx;
          mv->vectors[index].y = dy;
          mv->vectors[index].z = dz;
          mv->vectors[index].count = 1;
          mv->vectors[index].im1 = ID;
          }
        }
      } //end u
    } //end v
}

void SetVectorsSolidNonRigid(SURFACE surf, SURFACE surf2, int ID, MOTION_VECTORS *mv, int ave_flag, int factor, int ctrl_flag)
{
  int i, j, k, a, b, c, u, v, x, y, z;
  float uvar, vvar;
  POINT C, C2;
  float dx, dy, dz;
  unsigned index;
  int p_step, u_step, v_step;
  float radius, length;
  int max_dim;

  if( (surf.net.n != surf2.net.n) || (surf.net.m != surf2.net.m))
  {
	  printf("\nProblem in setvectors for %s %s %i %i  %i %i", surf.name, surf2.name, surf.net.n, surf.net.m, surf2.net.n, surf2.net.m);
	  OutputSurfRhino(surf, "t1.nrb");
	  OutputSurfRhino(surf2, "t2.nrb");
	  return;
  }

  if(mv->XDIM > mv->YDIM)
    max_dim = mv->XDIM;
  else
    max_dim = mv->YDIM;
  if(mv->ZDIM > max_dim)
    max_dim = mv->ZDIM;

  FindCenterline(&surf);
  FindCenterline(&surf2);

  length = FindLength(surf);
  v_step = floor(factor*length / mv->pixel_width + 0.5);

  radius = distance(surf.net.Pw[0][surf.net.m/2].x, surf.net.Pw[0][surf.net.m/2].y, surf.net.Pw[0][surf.net.m/2].z,
                   surf.centerline[surf.net.m/2].x, surf.centerline[surf.net.m/2].y, surf.centerline[surf.net.m/2].z);
  p_step = floor(factor*radius / mv->pixel_width + 0.5);

  if(v_step < 2)
    v_step = 5;

  if(p_step < 2)
    p_step = 5;

  initialize_structure(&vol, surf.net.n, surf.net.m);
  initialize_structure(&vol2, surf2.net.n, surf2.net.m);

  if(ctrl_flag)
  {
  for(i = 0; i < surf.net.m; i++)
    for(j = 0; j < surf.net.n; j++)
      {
      dx = surf2.net.Pw[j][i].x - surf.net.Pw[j][i].x;
      dy = surf2.net.Pw[j][i].y - surf.net.Pw[j][i].y;
      dz = surf2.net.Pw[j][i].z - surf.net.Pw[j][i].z;

      x = floor((surf.net.Pw[j][i].x - mv->xoff) / mv->pixel_width + 0.5);
      y = floor((surf.net.Pw[j][i].y - mv->yoff) / mv->pixel_width + 0.5);
      z = floor((surf.net.Pw[j][i].z - mv->zoff) / mv->slice_width + 0.5);

      for(c = z-1; c <= z+1; c++)
      for(b = y-1; b <= y+1; b++)
      for(a = x-1; a <= x+1; a++)
        {
        if(a >= 0 && a < mv->XDIM && b >= 0 && b < mv->YDIM && c >= 0 && c < mv->ZDIM)
          {
          index = a + b*mv->XDIM + c*mv->XDIM*mv->YDIM;

          if(ave_flag)
            {
            mv->vectors[index].x += dx;
            mv->vectors[index].y += dy;
            mv->vectors[index].z += dz;

            mv->vectors[index].count++;
            mv->vectors[index].im1 = ID;
            }
          else
            {
            mv->vectors[index].x = dx;
            mv->vectors[index].y = dy;
            mv->vectors[index].z = dz;


            mv->vectors[index].count = 1;
            mv->vectors[index].im1 = ID;
            }
          }
        }
      }
  }

  for(k = 0; k <= p_step; k++)
    {
    generate_structure(&vol, surf, surf.centerline, (float) k / p_step);
    generate_structure(&vol2, surf2, surf2.centerline, (float) k / p_step);

    for(v = 0; v <= v_step; v++)
      {
      vvar = (float) v / v_step;
      u_step = floor(2*factor*3.14*k/p_step*radius / mv->pixel_width);
      if(u_step == 0)
        u_step = 1;

      for(u = 0; u <= u_step; u++)
        {
        uvar = (float) u / u_step;

        C = SurfacePoint(vol.net.n-1, p_degree, vol.knu, vol.net.m-1, q_degree,
                         vol.knv, vol.net.Pw, uvar, vvar);
        C2 = SurfacePoint(vol2.net.n-1, p_degree, vol2.knu, vol2.net.m-1, q_degree,
                         vol2.knv, vol2.net.Pw, uvar, vvar);

        dx = C2.x - C.x;
        dy = C2.y - C.y;
        dz = C2.z - C.z;

        x = floor((C.x - mv->xoff) / mv->pixel_width + 0.5);
        y = floor((C.y - mv->yoff) / mv->pixel_width + 0.5);
        z = floor((C.z -mv->zoff) / mv->slice_width + 0.5);

        if(x >= 0 && x < mv->XDIM && y >= 0 && y < mv->YDIM && z >= 0 && z < mv->ZDIM)
          {
          index = x + y*mv->XDIM + z*mv->XDIM*mv->YDIM;
          if(ave_flag)
            {
            mv->vectors[index].x += dx;
            mv->vectors[index].y += dy;
            mv->vectors[index].z += dz;

            mv->vectors[index].count++;
            mv->vectors[index].im1 = ID;
            }
          else
            {
            mv->vectors[index].x = dx;
            mv->vectors[index].y = dy;
            mv->vectors[index].z = dz;
            mv->vectors[index].count = 1;
            mv->vectors[index].im1 = ID;
            }
		  }
	    }
      for(u = 0; u <= 20*factor; u++)
        {
        uvar = u/(20.0*factor)*0.025;

        C = SurfacePoint(vol.net.n-1, p_degree, vol.knu, vol.net.m-1, q_degree,
                         vol.knv, vol.net.Pw, uvar, vvar);
        C2 = SurfacePoint(vol2.net.n-1, p_degree, vol2.knu, vol2.net.m-1, q_degree,
                         vol2.knv, vol2.net.Pw, uvar, vvar);

        dx = C2.x - C.x;
        dy = C2.y - C.y;
        dz = C2.z - C.z;

        x = floor((C.x - mv->xoff) / mv->pixel_width + 0.5);
        y = floor((C.y - mv->yoff) / mv->pixel_width + 0.5);
        z = floor((C.z -mv->zoff) / mv->slice_width + 0.5);

        if(x >= 0 && x < mv->XDIM && y >= 0 && y < mv->YDIM && z >= 0 && z < mv->ZDIM)
          {
          index = x + y*mv->XDIM + z*mv->XDIM*mv->YDIM;
          if(ave_flag)
            {
            mv->vectors[index].x += dx;
            mv->vectors[index].y += dy;
            mv->vectors[index].z += dz;

            mv->vectors[index].count++;
            mv->vectors[index].im1 = ID;
            }
          else
            {
            mv->vectors[index].x = dx;
            mv->vectors[index].y = dy;
            mv->vectors[index].z = dz;

            mv->vectors[index].count = 1;
            mv->vectors[index].im1 = ID;
            }
          }
	    }
      for(u = 0; u <= 20*factor; u++)
        {
        uvar = 0.975 + u/(20.0*factor)*0.025;

        C = SurfacePoint(vol.net.n-1, p_degree, vol.knu, vol.net.m-1, q_degree,
                         vol.knv, vol.net.Pw, uvar, vvar);
        C2 = SurfacePoint(vol2.net.n-1, p_degree, vol2.knu, vol2.net.m-1, q_degree,
                         vol2.knv, vol2.net.Pw, uvar, vvar);

        dx = C2.x - C.x;
        dy = C2.y - C.y;
        dz = C2.z - C.z;

        x = floor((C.x - mv->xoff) / mv->pixel_width + 0.5);
        y = floor((C.y - mv->yoff) / mv->pixel_width + 0.5);
        z = floor((C.z -mv->zoff) / mv->slice_width + 0.5);

        if(x >= 0 && x < mv->XDIM && y >= 0 && y < mv->YDIM && z >= 0 && z < mv->ZDIM)
          {
          index = x + y*mv->XDIM + z*mv->XDIM*mv->YDIM;
          if(ave_flag)
            {
            mv->vectors[index].x += dx;
            mv->vectors[index].y += dy;
            mv->vectors[index].z += dz;

            mv->vectors[index].count++;
            mv->vectors[index].im1 = ID;
            }
          else
            {
            mv->vectors[index].x = dx;
            mv->vectors[index].y = dy;
            mv->vectors[index].z = dz;

            mv->vectors[index].count = 1;
            mv->vectors[index].im1 = ID;
            }
          }
	    }
      }
    }
}


float InterpX(float new_loc[4], MOTION_VECTORS mv)
{
  unsigned int index2;
  float intensity, intensity2, intensity3, intensity4;

  index2 = (int)new_loc[1] + (int)new_loc[2]*mv.XDIM + (int)new_loc[3]*mv.XDIM*mv.YDIM;

  intensity  = (1.0 - (new_loc[1] - (int)new_loc[1])) * mv.vectors[index2].x + (new_loc[1] - (int)new_loc[1]) * mv.vectors[index2+1].x;
  intensity2 = (1.0 - (new_loc[1] - (int)new_loc[1])) * mv.vectors[index2+mv.XDIM].x + (new_loc[1] - (int)new_loc[1]) * mv.vectors[index2+mv.XDIM+1].x;
  intensity3 = (1.0 - (new_loc[2] - (int)new_loc[2])) * intensity + (new_loc[2] - (int)new_loc[2]) * intensity2;

  intensity  = (1.0 - (new_loc[1] - (int)new_loc[1])) * mv.vectors[index2+mv.XDIM*mv.YDIM].x + (new_loc[1] - (int)new_loc[1]) * mv.vectors[index2+mv.XDIM*mv.YDIM+1].x;
  intensity2 = (1.0 - (new_loc[1] - (int)new_loc[1])) * mv.vectors[index2+mv.XDIM*mv.YDIM+mv.XDIM].x + (new_loc[1] - (int)new_loc[1]) * mv.vectors[index2+mv.XDIM*mv.YDIM+mv.XDIM+1].x;
  intensity4 = (1.0 - (new_loc[2] - (int)new_loc[2])) * intensity + (new_loc[2] - (int)new_loc[2]) * intensity2;

  intensity = (1.0 - (new_loc[3] - (int)new_loc[3])) * intensity3 + (new_loc[3] - (int)new_loc[3]) * intensity4;
  return intensity;
}

float InterpY(float new_loc[4], MOTION_VECTORS mv)
{
  unsigned int index2;
  float intensity, intensity2, intensity3, intensity4;

  index2 = (int)new_loc[1] + (int)new_loc[2]*mv.XDIM + (int)new_loc[3]*mv.XDIM*mv.YDIM;

  intensity  = (1.0 - (new_loc[1] - (int)new_loc[1])) * mv.vectors[index2].y + (new_loc[1] - (int)new_loc[1]) * mv.vectors[index2+1].y;
  intensity2 = (1.0 - (new_loc[1] - (int)new_loc[1])) * mv.vectors[index2+mv.XDIM].y + (new_loc[1] - (int)new_loc[1]) * mv.vectors[index2+mv.XDIM+1].y;
  intensity3 = (1.0 - (new_loc[2] - (int)new_loc[2])) * intensity + (new_loc[2] - (int)new_loc[2]) * intensity2;

  intensity  = (1.0 - (new_loc[1] - (int)new_loc[1])) * mv.vectors[index2+mv.XDIM*mv.YDIM].y + (new_loc[1] - (int)new_loc[1]) * mv.vectors[index2+mv.XDIM*mv.YDIM+1].y;
  intensity2 = (1.0 - (new_loc[1] - (int)new_loc[1])) * mv.vectors[index2+mv.XDIM*mv.YDIM+mv.XDIM].y + (new_loc[1] - (int)new_loc[1]) * mv.vectors[index2+mv.XDIM*mv.YDIM+mv.XDIM+1].y;
  intensity4 = (1.0 - (new_loc[2] - (int)new_loc[2])) * intensity + (new_loc[2] - (int)new_loc[2]) * intensity2;

  intensity = (1.0 - (new_loc[3] - (int)new_loc[3])) * intensity3 + (new_loc[3] - (int)new_loc[3]) * intensity4;
  return intensity;
}

float InterpZ(float new_loc[4], MOTION_VECTORS mv)
{
  unsigned int index2;
  float intensity, intensity2, intensity3, intensity4;

  index2 = (int)new_loc[1] + (int)new_loc[2]*mv.XDIM + (int)new_loc[3]*mv.XDIM*mv.YDIM;

  intensity  = (1.0 - (new_loc[1] - (int)new_loc[1])) * mv.vectors[index2].z + (new_loc[1] - (int)new_loc[1]) * mv.vectors[index2+1].z;
  intensity2 = (1.0 - (new_loc[1] - (int)new_loc[1])) * mv.vectors[index2+mv.XDIM].z + (new_loc[1] - (int)new_loc[1]) * mv.vectors[index2+mv.XDIM+1].z;
  intensity3 = (1.0 - (new_loc[2] - (int)new_loc[2])) * intensity + (new_loc[2] - (int)new_loc[2]) * intensity2;

  intensity  = (1.0 - (new_loc[1] - (int)new_loc[1])) * mv.vectors[index2+mv.XDIM*mv.YDIM].z + (new_loc[1] - (int)new_loc[1]) * mv.vectors[index2+mv.XDIM*mv.YDIM+1].z;
  intensity2 = (1.0 - (new_loc[1] - (int)new_loc[1])) * mv.vectors[index2+mv.XDIM*mv.YDIM+mv.XDIM].z + (new_loc[1] - (int)new_loc[1]) * mv.vectors[index2+mv.XDIM*mv.YDIM+mv.XDIM+1].z;
  intensity4 = (1.0 - (new_loc[2] - (int)new_loc[2])) * intensity + (new_loc[2] - (int)new_loc[2]) * intensity2;

  intensity = (1.0 - (new_loc[3] - (int)new_loc[3])) * intensity3 + (new_loc[3] - (int)new_loc[3]) * intensity4;
  return intensity;
}

void TransformSurf(SURFACE *surf, MOTION_VECTORS mv)
{
  int i, j;
  unsigned index;
  int x, y, z;
  float dx, dy, dz;
  float new_loc[4];

  for(i = 0; i < surf->net.m; i++)
    {
    for(j = 0; j < surf->net.n; j++)
      {
      x = floor((surf->net.Pw[j][i].x - mv.xoff) / mv.pixel_width + 0.5);
      y = floor((surf->net.Pw[j][i].y - mv.yoff) / mv.pixel_width + 0.5);
      z = floor((surf->net.Pw[j][i].z - mv.zoff) / mv.slice_width + 0.5);

      new_loc[1] = (surf->net.Pw[j][i].x - mv.xoff) / mv.pixel_width;
      new_loc[2] = (surf->net.Pw[j][i].y - mv.yoff) / mv.pixel_width;
      new_loc[3] = (surf->net.Pw[j][i].z - mv.zoff) / mv.slice_width;

      if(x >= 1 && x < mv.XDIM-1 && y >= 1 && y < mv.YDIM-1 && z >= 1 && z < mv.ZDIM-1)
        {
        index = x + y*mv.XDIM + z*mv.XDIM*mv.YDIM;

        dx = InterpX(new_loc, mv);
        dy = InterpY(new_loc, mv);
        dz = InterpZ(new_loc, mv);
        }
      else
        {
        dx = 0.0;
        dy = 0.0;
        dz = 0.0;
        }

    surf->net.Pw[j][i].x += dx;
    surf->net.Pw[j][i].y += dy;
    surf->net.Pw[j][i].z += dz;
    }

  surf->net.Pw[surf->net.n-1][i].x = surf->net.Pw[0][i].x;
  surf->net.Pw[surf->net.n-1][i].y = surf->net.Pw[0][i].y;
  surf->net.Pw[surf->net.n-1][i].z = surf->net.Pw[0][i].z;
  }

  i = 0;
  for(j = 0; j < surf->net.n; j++)
    {
    surf->net.Pw[j][i].x = surf->net.Pw[0][i].x;
    surf->net.Pw[j][i].y = surf->net.Pw[0][i].y;
    surf->net.Pw[j][i].z = surf->net.Pw[0][i].z;
    }

  i = surf->net.m-1;
  for(j = 0; j < surf->net.n; j++)
    {
    surf->net.Pw[j][i].x = surf->net.Pw[0][i].x;
    surf->net.Pw[j][i].y = surf->net.Pw[0][i].y;
    surf->net.Pw[j][i].z = surf->net.Pw[0][i].z;
    }
}

void CheckVector(MOTION_VECTORS *mv, int ID, int x, int y, int z)
{
  float X, Y, Z;
  int iX, iY, iZ;
  float dx, dy, dz;
  unsigned int index;
  float mag;
  int i, end, found;
  float dist;
  int foundID = 0;

  index = x + y*mv->XDIM + z*mv->XDIM*mv->YDIM;

  dx = mv->vectors[index].x / (mv->pixel_width);
  dy = mv->vectors[index].y / (mv->pixel_width);
  dz = mv->vectors[index].z / (mv->slice_width);

  mag = sqrt(dx*dx+dy*dy+dz*dz);

  if(mag != 0)
    {
    dx /= mag;
    dy /= mag;
    dz /= mag;
    }
  else
    {
    dx = 0;
    dy = 0;
    dz = 0;
    }
  end = floor(mag + 0.5);

  X = x;
  Y = y;
  Z = z;
  found = 0;
  i = 0;
  dist = 0;

  while(!found && i < end)
    {
    X += dx;
    Y += dy;
    Z += dz;

    iX = Round(X);
    iY = Round(Y);
    iZ = Round(Z);

    if(iX >= 0 && iX < mv->XDIM && iY >= 0 && iY < mv->YDIM && iZ >= 0 && iZ < mv->ZDIM)
      {
      index = iX + iY*mv->XDIM + iZ*mv->XDIM*mv->YDIM;

      if(mv->vectors[index].im1 != ID && mv->vectors[index].im1 >= 0)
        {
        dist = i;
        found = 1;
        foundID = mv->vectors[index].im1;
        }
      }
    i++;
    }

  if(found && dist >= 0)  //adjust vector
    {
    index = x + y*mv->XDIM + z*mv->XDIM*mv->YDIM;
    mv->vectors[index].x = (dist-0.5)*dx*mv->pixel_width;
    mv->vectors[index].y = (dist-0.5)*dy*mv->pixel_width;
    mv->vectors[index].z = (dist-0.5)*dz*mv->slice_width;
    }
}

void Scale_organ_centerline(SURFACE *nrb_model, float scale)
{
  int i, j;

  nrb_model->flag = 1;
  FindCenterline(nrb_model);

  for(i = 0; i < nrb_model->net.m; i++)
    for(j = 0; j < nrb_model->net.n; j++)
      {
      nrb_model->net.Pw[j][i].x -= nrb_model->centerline[i].x;
      nrb_model->net.Pw[j][i].y -= nrb_model->centerline[i].y;
      nrb_model->net.Pw[j][i].z -= nrb_model->centerline[i].z;

      nrb_model->net.Pw[j][i].x *= scale;
      nrb_model->net.Pw[j][i].y *= scale;
      nrb_model->net.Pw[j][i].z *= scale;

      nrb_model->net.Pw[j][i].x += nrb_model->centerline[i].x;
      nrb_model->net.Pw[j][i].y += nrb_model->centerline[i].y;
      nrb_model->net.Pw[j][i].z += nrb_model->centerline[i].z;
      }
}

float SmoothAllVectorsBut(int ID, MOTION_VECTORS *mv)
{
  int x, y, z;
  int i, j, k;
  float avex, avey, avez;
  int count;
  unsigned index1, index2;
  float ave_change;
  unsigned int count2;

  printf("\nIn smooth!");
  ave_change = 0.0;
  count2 = 0;
  for(z = 0; z < mv->ZDIM; z++)
    for(y = 0; y < mv->YDIM; y++)
      for(x = 0; x < mv->XDIM; x++)
        {
        index1 = x + y*mv->XDIM + z*mv->XDIM*mv->YDIM;

        tmp_pts[index1].x = mv->vectors[index1].x;
        tmp_pts[index1].y = mv->vectors[index1].y;
        tmp_pts[index1].z = mv->vectors[index1].z;

        if(mv->vectors[index1].im1 != ID)
          {
          /* Smooth the vectors according to its neighbors */
          count = 0;
          avex = 0; avey = 0; avez = 0;

          for(k = z-1; k <= z+1; k++)
            for(j = y-1; j <= y+1; j++)
              for(i = x-1; i <= x+1; i++)
                {
                if(i >= 0 && i < mv->XDIM && j >= 0 && j < mv->YDIM && k >= 0 && k < mv->ZDIM)
                  {
                  index2 = i + j*mv->XDIM + k*mv->XDIM*mv->YDIM;
                  avex += mv->vectors[index2].x;
                  avey += mv->vectors[index2].y;
                  avez += mv->vectors[index2].z;
                  count++;
                  }
                }

          if(count > 0)
            {
            avex /= count;
            avey /= count;
            avez /= count;
            }

          ave_change += fabs(mv->vectors[index1].x - avex);
          ave_change += fabs(mv->vectors[index1].y - avey);
          ave_change += fabs(mv->vectors[index1].z - avez);
          count2 += 3;

          tmp_pts[index1].x = avex;
          tmp_pts[index1].y = avey;
          tmp_pts[index1].z = avez;
		  }
        }

  for(index1 = 0; index1 < mv->XDIM*mv->YDIM*mv->ZDIM; index1++)
    {
	mv->vectors[index1].x = tmp_pts[index1].x;
	mv->vectors[index1].y = tmp_pts[index1].y;
	mv->vectors[index1].z = tmp_pts[index1].z;
    }

  if(count2 > 0)
    ave_change /= count2;
  return ave_change;
}

void ExpandIntest(MOTION_VECTORS mv) //Used to expand small intestine centerline
{
  int i, j, k, l, ID, ID2, ID_match;
  float error, min_error;
  unsigned int index;
  float minx, maxx, miny, maxy, minz, maxz;
  float change;
  float num2;

  for(index = 0; index < mv.XDIM*mv.YDIM*mv.ZDIM; index++)
    {
    mv.vectors[index].x = 0;
    mv.vectors[index].y = 0;
    mv.vectors[index].z = 0;
    mv.vectors[index].count = 0;
    mv.vectors[index].im1 = -1;
    }

  printf("\nBefore centerline!");
  SetVectorsSolidNonRigid(sm_intest, sm_intest_exp, 10, &mv, 1, 1, 1);

  for(index = 0; index < mv.XDIM*mv.YDIM*mv.ZDIM; index++)
    {
    if(mv.vectors[index].surf_flag == 1)
	  {
      mv.vectors[index].x = 0;
      mv.vectors[index].y = 0;
      mv.vectors[index].z = 0;
      mv.vectors[index].count = 1;
      mv.vectors[index].im1 = 20;
	  }
    }

  for(index = 0; index < mv.XDIM*mv.YDIM*mv.ZDIM; index++)
    {
	if(mv.vectors[index].count > 0)
	  {
      mv.vectors[index].x /= mv.vectors[index].count;
      mv.vectors[index].y /= mv.vectors[index].count;
      mv.vectors[index].z /= mv.vectors[index].count;
	  }
    }

  for(k = 0; k < mv.ZDIM; k++)
    for(j = 0; j < mv.YDIM; j++)
      for(i = 0; i < mv.XDIM; i++)
        {
        index = i + j*mv.XDIM + k*mv.XDIM*mv.YDIM;
        if(mv.vectors[index].im1 == 0)
          CheckVector(&mv, 10, i, j, k);
        }

 for(i = 0; i < 15; i++)
    {
	printf("\nSmoothing for Organ Transform %i", i);
    change = SmoothAllVectorsBut(20, &mv);
    }

  i = 0;
  change = 1.0;
  while(change > 0.01 && i < 100) 
    {
	printf("\nSmoothing for Organ Transform %i", i);
    change = SmoothAllVectorsBut(20, &mv);
    i++;
    }

  TransformSurf(&sm_intest, mv);
  printf("\nAfter transform!");
}

void MeshSurface(SURFACE nrb_model, SURFACE *tmodel, int N, int M)
{
  POINT C, C2, C3, C4;
  int i, j;
  float uvar, vvar;
  float ustep, vstep;
  unsigned long int count;

  uvar = 0.0; vvar = 0.0;
  ustep = 1.0 / (float)N;
  vstep = 1.0 / (float)M;

  printf("\nBefore allocation!");
  tmodel->net.n = 3;
  tmodel->net.m = N*M*2;
  initialize_structure(tmodel, tmodel->net.n, tmodel->net.m);

  printf("\nAfter allocation!");

  count = 0;
  for(i = 0; i <= N-1; i+= 1)
    {
    uvar = (float)i / N;
    for(j = 0; j <= M-1; j+= 1)
      {
      vvar = (float)j / M;

      if(i == N-1 && j!= M-1)
        {
        C = SurfacePoint(nrb_model.net.n-1, 3, nrb_model.knu, nrb_model.net.m-1, 3, nrb_model.knv, nrb_model.net.Pw, uvar, vvar);
        C2 = SurfacePoint(nrb_model.net.n-1, 3, nrb_model.knu, nrb_model.net.m-1, 3, nrb_model.knv, nrb_model.net.Pw, 1, vvar);
        C3 = SurfacePoint(nrb_model.net.n-1, 3, nrb_model.knu, nrb_model.net.m-1, 3, nrb_model.knv, nrb_model.net.Pw, uvar, vvar+vstep);
        C4 = SurfacePoint(nrb_model.net.n-1, 3, nrb_model.knu, nrb_model.net.m-1, 3, nrb_model.knv, nrb_model.net.Pw, 1, vvar+vstep);
        }
      else if(j == M-1 && i != N-1)
        {
        C = SurfacePoint(nrb_model.net.n-1, 3, nrb_model.knu,  nrb_model.net.m-1, 3, nrb_model.knv, nrb_model.net.Pw, uvar, vvar);
        C2 = SurfacePoint(nrb_model.net.n-1, 3, nrb_model.knu, nrb_model.net.m-1, 3, nrb_model.knv, nrb_model.net.Pw, uvar+ustep, vvar);
        C3 = SurfacePoint(nrb_model.net.n-1, 3, nrb_model.knu, nrb_model.net.m-1, 3, nrb_model.knv, nrb_model.net.Pw, uvar, 1);
        C4 = SurfacePoint(nrb_model.net.n-1, 3, nrb_model.knu, nrb_model.net.m-1, 3, nrb_model.knv, nrb_model.net.Pw, uvar+ustep, 1);
        }
      else if(j == M-1 && i == N-1)
        {
        C = SurfacePoint(nrb_model.net.n-1, 3, nrb_model.knu, nrb_model.net.m-1, 3, nrb_model.knv, nrb_model.net.Pw, uvar, vvar);
        C2 = SurfacePoint(nrb_model.net.n-1, 3, nrb_model.knu, nrb_model.net.m-1, 3, nrb_model.knv, nrb_model.net.Pw, 1, vvar);
        C3 = SurfacePoint(nrb_model.net.n-1, 3, nrb_model.knu, nrb_model.net.m-1, 3, nrb_model.knv, nrb_model.net.Pw, uvar, 1);
        C4 = SurfacePoint(nrb_model.net.n-1, 3, nrb_model.knu, nrb_model.net.m-1, 3, nrb_model.knv, nrb_model.net.Pw, 1, 1);
        }
      else
        {
        C = SurfacePoint(nrb_model.net.n-1, 3, nrb_model.knu, nrb_model.net.m-1, 3, nrb_model.knv, nrb_model.net.Pw, uvar, vvar);
        C2 = SurfacePoint(nrb_model.net.n-1, 3, nrb_model.knu, nrb_model.net.m-1, 3, nrb_model.knv, nrb_model.net.Pw, uvar+ustep, vvar);
        C3 = SurfacePoint(nrb_model.net.n-1, 3, nrb_model.knu, nrb_model.net.m-1, 3, nrb_model.knv, nrb_model.net.Pw, uvar, vvar+vstep);
        C4 = SurfacePoint(nrb_model.net.n-1, 3, nrb_model.knu, nrb_model.net.m-1, 3, nrb_model.knv, nrb_model.net.Pw, uvar+ustep, vvar+vstep);
        }

	  tmodel->net.Pw[0][count].x = C.x;
	  tmodel->net.Pw[0][count].y = C.y;
	  tmodel->net.Pw[0][count].z = C.z;

	  tmodel->net.Pw[1][count].x = C2.x;
	  tmodel->net.Pw[1][count].y = C2.y;
	  tmodel->net.Pw[1][count].z = C2.z;

	  tmodel->net.Pw[2][count].x = C3.x;
	  tmodel->net.Pw[2][count].y = C3.y;
	  tmodel->net.Pw[2][count].z = C3.z;

	  count++;

	  tmodel->net.Pw[0][count].x = C3.x;
	  tmodel->net.Pw[0][count].y = C3.y;
	  tmodel->net.Pw[0][count].z = C3.z;

	  tmodel->net.Pw[1][count].x = C2.x;
	  tmodel->net.Pw[1][count].y = C2.y;
	  tmodel->net.Pw[1][count].z = C2.z;

	  tmodel->net.Pw[2][count].x = C4.x;
	  tmodel->net.Pw[2][count].y = C4.y;
	  tmodel->net.Pw[2][count].z = C4.z;

	  count++;
      }
    }
}

void ReadBinarySTL(char *filename, SURFACE *nrb_model) 
{
  FILE *fp;
  unsigned char header[80];
  int i, num_tri;
  float vertex1[3], vertex2[3], vertex3[3];
  float normal[3];
  unsigned short attrByteCount;

  fp = fopen(filename, "rb");
  if (!fp) 
    {
    printf("Error opening file: %s\n", filename);
    exit(1);
    }

  // Read the file header (80 bytes)
  fread(header, sizeof(unsigned char), 80, fp);

  // Read the number of triangles (4 bytes)
  fread(&num_tri, sizeof(int), 1, fp);

  sprintf(nrb_model->name, "tmod_sm_intest_boundary");

  nrb_model->type = 1;
  nrb_model->net.m = num_tri;
  nrb_model->net.n = 3;

  initialize_structure(nrb_model, nrb_model->net.n, nrb_model->net.m);

  // Read each triangle
  for(i = 0; i < num_tri; i++) 
    {
    // Read the normal vector (12 bytes)
    fread(normal, sizeof(float), 3, fp);

    // Read the vertex coordinates (36 bytes)
    fread(vertex1, sizeof(float), 3, fp);
    fread(vertex2, sizeof(float), 3, fp);
    fread(vertex3, sizeof(float), 3, fp);

    // Read the attribute byte count (2 bytes)
    fread(&attrByteCount, sizeof(unsigned short), 1, fp);

    nrb_model->net.Pw[0][i].x = vertex1[0];
    nrb_model->net.Pw[0][i].y = vertex1[1];
    nrb_model->net.Pw[0][i].z = vertex1[2];

    nrb_model->net.Pw[1][i].x = vertex2[0];
    nrb_model->net.Pw[1][i].y = vertex2[1];
    nrb_model->net.Pw[1][i].z = vertex2[2];

    nrb_model->net.Pw[2][i].x = vertex3[0];
    nrb_model->net.Pw[2][i].y = vertex3[1];
    nrb_model->net.Pw[2][i].z = vertex3[2];
    }

  fclose(fp);
}



/***********************/
/* BEGIN MAIN PROGRAM  */
/***********************/

main(int argc, char *argv[])
{
  char surface_file[200], bounding_surface_file[200], output_file[200], filename[200];
  int i, j;
  unsigned long index;
  float minx, maxx, miny, maxy, minz, maxz;
  float small_int_scale = 1.0;
  float sm_int_init_vol, sm_int_des_vol;
  int num = 300;
  MOTION_VECTORS mv;
  float *out_pixel;
  float length, ave_radius, min_radius, max_radius;
  FILE *fp;

  initialize_structure(&vol, 300, 950);
  initialize_structure(&vol2, 300, 950);
  initialize_structure(&tmp_surf, 300, 950);
  slice_tmodel.tris = tri_vector(0, 200000);
  tmp_pts = p_vector(0, 512*512*700);

/* Interpret options */
/* Go through each part of the command line, find options preceded by
        a hyphen, and read variables using the function atof, where
        appropriate. */
      
        while (--argc > 0 && (*++argv)[0] == '-') {
                char    *s;
                for (s = argv[0] + 1; *s; s++)
                        switch (*s) {

						case 'v':
								sm_int_des_vol = atof(*++argv);
								argc--;
								break;

                        default:
                                fprintf (stderr, "Unknown option: -%c\n", *s);
                                Abort ("");
                                break;
                        }
        }

        if (argc < 1) {
                fprintf(stderr,
                    "Usage: grow_sm_intest [options] CSV_surface_file STL_bounding_surface_file output_file \n\n");
                fprintf(stderr,
                    "       [-v] desired_vol         desired small intestine volume (mls)\n");
                fprintf(stderr,
                    "       [-D]          Print debugging information\n");
                exit(1);
        }

  /* Get image file name from argument list */
  if (sscanf(*argv, "%s", surface_file) == 0)
          Abort("Can not get surface_file filename");
  if (sscanf(*++argv, "%s", bounding_surface_file) == 0)
          Abort("Can not get bounding_surface_STL filename");
  if (sscanf(*++argv, "%s", output_file) == 0)
          Abort("Can not get output filename");

  printf("\nReading small intestine surface file...");
  Read_small_intest_surf(surface_file);
  //OutputSurfRhino(sm_intest, "t.nrb");

  printf("\nReading bounding STL surface file...");
  ReadBinarySTL(bounding_surface_file, &sm_intest_boundary);

  Calc_extents(&sm_intest_boundary);
  minx = sm_intest_boundary.min_x;
  maxx = sm_intest_boundary.max_x;
  miny = sm_intest_boundary.min_y;
  maxy = sm_intest_boundary.max_y;
  minz = sm_intest_boundary.min_z;
  maxz = sm_intest_boundary.max_z;

  if((maxx - minx) > (maxy - miny) )
    mv.pixel_width = (maxx-minx) / num;
  else
    mv.pixel_width = (maxy-miny) / num;
  mv.slice_width = mv.pixel_width;

  mv.xoff = minx-10*mv.pixel_width;
  mv.yoff = miny-10*mv.pixel_width;
  mv.zoff = minz-10*mv.slice_width;

  mv.XDIM = floor( (maxx - mv.xoff) / mv.pixel_width + 0.5)+10;
  mv.YDIM = floor( (maxy - mv.yoff) / mv.pixel_width + 0.5)+10;
  mv.ZDIM = floor( (maxz - mv.zoff) / mv.slice_width + 0.5)+10;

  printf("\nImage Dims = %i %i %i", mv.XDIM, mv.YDIM, mv.ZDIM);
  mv.vectors = vp_vector(0, mv.XDIM*mv.YDIM*mv.ZDIM-1);

  xdim = mv.XDIM; ydim = mv.YDIM; zdim = mv.ZDIM;
  for(index = 0; index < mv.XDIM*mv.YDIM*mv.ZDIM; index++)
      {
      mv.vectors[index].x = 0;
      mv.vectors[index].y = 0;
      mv.vectors[index].z = 0;
      mv.vectors[index].count = 0;
      mv.vectors[index].im1 = -1;
      }

  MeshSurface(sm_intest, &tri_sm_intest, 100, 900);
  sm_int_init_vol = Render_Tri_Model(&mv, tri_sm_intest, 20);

  printf("\nInit Volume = %f", sm_int_init_vol);
  printf("\nDesired volume = %f", sm_int_des_vol);

  initialize_structure(&sm_intest_exp, sm_intest.net.n, sm_intest.net.m);
  CopySurface(&sm_intest_exp, &sm_intest);
  small_int_scale = 1.5*sqrt(sm_int_des_vol / sm_int_init_vol); //Added factor of 1.5 just to go a little higher
  printf("\nScaling factor = %f", small_int_scale);

  Scale_organ_centerline(&sm_intest_exp, small_int_scale);  

  for(index = 0; index < mv.XDIM*mv.YDIM*mv.ZDIM; index++)
      {
      mv.vectors[index].x = 0;
      mv.vectors[index].y = 0;
      mv.vectors[index].z = 0;
      mv.vectors[index].count = 0;
      mv.vectors[index].im1 = -1;
      }

  Render_Tri_Model(&mv, sm_intest_boundary, 20);

  //out_pixel = vector(0, mv.XDIM*mv.YDIM*mv.ZDIM-1);
  //for(index = 0; index < mv.XDIM*mv.YDIM*mv.ZDIM; index++)
  //  out_pixel[index] = mv.vectors[index].im1;

  //SaveImage("bob.bin", mv.XDIM, mv.YDIM, mv.ZDIM, out_pixel);

  for(index = 0; index < mv.XDIM*mv.YDIM*mv.ZDIM; index++)
    {
	if(mv.vectors[index].im1 != 20)
	  {
	  mv.vectors[index].x = 0.0;
	  mv.vectors[index].y = 0.0;
	  mv.vectors[index].z = 0.0;
	  mv.vectors[index].surf_flag = 1;
	  }
	else
	  mv.vectors[index].surf_flag = 0;
    }

  for(i = 1; i <= 10; i++)
    ExpandIntest(mv);

  if((fp = fopen(output_file, "w")) == NULL)
    Abort("Can not open output for Rhino file");
  fclose(fp);

  FindCenterline(&sm_intest);
  length = FindLength(sm_intest);
  FindRadiusStats(sm_intest, &ave_radius, &min_radius, &max_radius);

  printf("\nLength of small intestine: %f", length);
  printf("\nAve diameter: %f", 2*ave_radius);

  OutputSurfRhino(sm_intest, output_file);
} /*	End Main	*/


//Code for bounding surfaces with more than 1 surface

//  Read_bounding_file(bounding_models, bounding_surface_file);
//  printf("\nNum_models = %i", NUM_MODELS);

/*
  minx = 1000000;
  miny = 1000000;
  minz = 1000000;
  maxx = -1000000;
  maxy = -1000000;
  maxz = -1000000;
  for(i = 0; i < NUM_MODELS; i++) 
    {
    Calc_extents(&bounding_models[i]);
    if(bounding_models[i].min_x < minx)
      minx = bounding_models[i].min_x;

    if(bounding_models[i].max_x > maxx)
      maxx = bounding_models[i].max_x;

    if(bounding_models[i].min_y < miny)
      miny = bounding_models[i].min_y;

    if(bounding_models[i].max_y > maxy)
      maxy = bounding_models[i].max_y;

    if(bounding_models[i].min_z < minz)
      minz = bounding_models[i].min_z;

    if(bounding_models[i].max_z > maxz)
      maxz = bounding_models[i].max_z;
    }

  if((maxx - minx) > (maxy - miny) )
    mv.pixel_width = (maxx-minx) / num;
  else
    mv.pixel_width = (maxy-miny) / num;
  mv.slice_width = mv.pixel_width;

  mv.xoff = minx-10*mv.pixel_width;
  mv.yoff = miny-10*mv.pixel_width;
  mv.zoff = minz-10*mv.slice_width;

  mv.XDIM = floor( (maxx - mv.xoff) / mv.pixel_width + 0.5)+10;
  mv.YDIM = floor( (maxy - mv.yoff) / mv.pixel_width + 0.5)+10;
  mv.ZDIM = floor( (maxz - mv.zoff) / mv.slice_width + 0.5)+10;

  printf("\nMV Dims = %i %i %i", mv.XDIM, mv.YDIM, mv.ZDIM);
  mv.vectors = vp_vector(0, mv.XDIM*mv.YDIM*mv.ZDIM-1);

  xdim = mv.XDIM; ydim = mv.YDIM; zdim = mv.ZDIM;
  for(index = 0; index < mv.XDIM*mv.YDIM*mv.ZDIM; index++)
      {
      mv.vectors[index].x = 0;
      mv.vectors[index].y = 0;
      mv.vectors[index].z = 0;
      mv.vectors[index].count = 0;
      mv.vectors[index].im1 = -1;
      }

  printf("\nBefore bounding surfaces");
  for(i = 0; i < NUM_MODELS; i++)
    {
	if(bounding_models[i].type == 0)
      RenderInVectors2(bounding_models[i], bez_bounding_models[i], &mv, 20);
	else
      Render_Tri_Model(&mv, bounding_models[i], 20);
    }
*/