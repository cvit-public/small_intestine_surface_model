typedef long INDEX;
typedef short FLAG;
typedef int INTEGER;
typedef char * STRING;
typedef short DEGREE;
typedef double PARAMETER;

#define NR_END 1
//#define SWAP(a,b) itemp=(a); (a) = (b); (b) = itemp;
#define NSTACK 50
#define FREE_ARG char*


/*Added to try new way*/
typedef struct line_seg
{
  double x1, x2;
  int organ_id;
} LINE_SEG;

/*Added to try new way*/
typedef struct seg_array
{
  int length;
  LINE_SEG sp[100];
} SEG_ARRAY;

typedef struct point
{
  float x;
  float y;
  float z;
} POINT;


typedef struct triangle
{
  POINT vertex[3];
} TRIANGLE;

typedef struct tri_model
{
  TRIANGLE *tris;
  int num_tris;
  char name[200];
  float min_x, max_x, min_y, max_y, min_z, max_z;
  float centerx, centery, centerz;
} TRI_MODEL;

typedef struct bezier_patch
{
  double cntrl_points[4][4][3];
  double slice_points[4][4][3];

  int slice_minz, slice_maxz;
  double minz, maxz, miny, maxy, minx, maxx;
} BEZIER_PATCH;

typedef struct bezier_model
{
  BEZIER_PATCH *patches;
  int num_patches;
} BEZIER_MODEL;


typedef struct qpoints
{
  INDEX n;
  POINT *Qw;
} QPOINTS;

typedef struct cpoint
{
  float  x,
         y,
         z, 
         w;
} CPOINT;

typedef struct vpoint
{
  float  x,
         y,
         z;

  int    im1;
  int    count;
  int    surf_flag;
} VPOINT;

typedef struct motion_vectors
{
  VPOINT *vectors;
  int XDIM, YDIM, ZDIM;
  float pixel_width, slice_width;
  float xoff, yoff, zoff;
} MOTION_VECTORS;

typedef struct cpolygon
{
  INDEX n;
  CPOINT *Pw;
} CPOLYGON;

typedef struct knotvector
{
    INDEX m;
    float *U;
} KNOTVECTOR;

typedef struct curve
{
  CPOLYGON pol;
  DEGREE p;
  KNOTVECTOR knt;
  float *uk;
} CURVE;

typedef struct cnet
{
  INDEX n,
        m;
  CPOINT **Pw;
} CNET;

typedef struct qnet
{
  INDEX n,
        m;
  POINT **Qw;
} QNET;

typedef struct surface
{
  CNET net;
  DEGREE p,
         q;
  KNOTVECTOR knu, 
             knv;
  float min_x, max_x, min_y, max_y, min_z, max_z;
  float xmax, ymax, zmax;
  float xmin, ymin, zmin;

  float xy_span, z_span;
  char name[200];
  int ID;
  int match1, match2;
  int flag;
  int type;

  POINT **orig_Pw;
  POINT *centerline;
  float vol;
} SURFACE;

typedef struct organ
{
  SURFACE surf[400];
  int num;
} ORGAN;

typedef struct org_model
{
  int type; //0 = surface, 1 = tri_model
  SURFACE nrb_model;
  BEZIER_MODEL bez_model;
  TRI_MODEL tmodel;
  char name[200];
  int mu_id[10];
  float mu_frac[10];
  int num_mat;
  float atten;
  double vol;
} ORG_MODEL;

typedef struct xpoint
{
  double x;
  int organ_id;
} XPOINT;

typedef struct xp_array
{
  XPOINT xp[100];
  int length;
} XP_ARRAY;


/*NURBS_BEZ.H*/
#include <stdio.h>
#include <assert.h>

#define MAX_ORDER 20

typedef unsigned int boolean;

/* Declaration of a Nurb surface patch */
typedef struct {
  int             numU, numV;   /* #points in U and V directions */
  int             ordU, ordV;   /* order of the spline in U and V */
  float          *kU, *kV;      /* knot vectors */
                                /* length(kU) = [0...numU - 1 + ordU] */
                                /* length(kV) = [0...numV - 1 + ordV] */
  CPOINT         **points;      /* [0..numV - 1][0..numU -1] array */
} patch;

/* Structure to hold the statistics */
typedef struct stat_s {
  int count;                    /* number of patches */
  int nbezs;                    /* #Bezier patches generated */
  int tot_nbezs;                /* Total #Bez patches for the whole */
  int tot_trimpts1;             /* Max PW Trim Curve Size */
  int tot_trimpts2;             /* Max SPLINE Trim Curve Size */
                                /* file  */
} statistics_t;

