"""
Step 1.

We convert our input files from nii.gz to stl files
"""

import argparse  # <-- Import the argparse module
import SimpleITK as sitk
import numpy as np
import pygalmesh
from pathlib import Path
from tqdm import tqdm

def read_sitk(file_path):
    '''
    Returns the numpy array and spacing for simple itk image path
    '''
    sitk_img = sitk.ReadImage(file_path)
    spacing = sitk_img.GetSpacing()
    img_array = (sitk.GetArrayFromImage(sitk_img)).astype(np.uint8)
    img_array = np.swapaxes(img_array, 0,2)   #Numpy loads the SimpleITK image in z, y x directions.
    return img_array, spacing

def convert_nii_gz_to_stl(directory):  # <-- Add the directory parameter
    """
    Convert all nii.gz files in the given directory to .stl
    The results are stored in folder `data_1_nii_to_stl`
    """
    directory = Path(directory)  # <-- Use the passed directory

    # List all .nii.gz files in the directory and its subdirectories
    nii_files = list(directory.rglob('*.nii.gz'))

    # convert the files
    for nifty_file in tqdm(nii_files):
        img_array,spacing = read_sitk(nifty_file)
        facet_distance = 1
        img_array_bool = img_array.astype(bool)
        if np.any(img_array_bool):
            current_mesh= pygalmesh.generate_from_array(img_array, voxel_size = spacing, max_facet_distance=facet_distance)
            save_path = list(nifty_file.with_suffix('').with_suffix('.stl').parts)
            save_path[-3] = 'data'
            save_path = Path(*save_path)
            save_path.parent.mkdir(parents=True, exist_ok=True)
            current_mesh.write(save_path)


if __name__ == '__main__':
    # Argument parser to get the directory from CLI
    parser = argparse.ArgumentParser(description="Convert nii.gz files in a specified directory to .stl.")
    parser.add_argument("directory", type=str, help="The directory containing nii.gz files.")
    args = parser.parse_args()
    
    print('directory', args.directory)
    convert_nii_gz_to_stl(args.directory)

