# Use the latest Debian image as a base
# FROM python:3.10
FROM --platform=linux/amd64 python:3.10

RUN apt-get update && apt-get install -y \
    libcgal-dev \
    libeigen3-dev \
    neovim \
    libboost-system-dev \
    libboost-filesystem-dev \
    cmake \
    blender \
    && rm -rf /var/lib/apt/lists/*

COPY step_1_nii_to_stl.py \
     step_2_calculate_volume.py \
     step_4_start_end_points.py \
     step_5_small_intestine_random_walk.py \
     step_6_convert_result.py \
     run.sh \
     README.md \
     /root/

COPY step_3_cgal/ /root/step_3_cgal/
COPY step_7_grow_sm_intest_new/ /root/step_7_grow_sm_intest_new/

# Install some Python libraries
RUN pip3 install pygalmesh SimpleITK numpy pandas tqdm pyarrow fastparquet \
  trimesh rtree scipy shapely mapbox_earcut scipy 'pyglet<2'
