import numpy as np
from scipy.interpolate import CubicSpline
import re
from pathlib import Path

pattern = r"length_(\d+\.\d+)-distance"


# Specify the directory
directory = Path('./data')

# List all folders
folders = [item for item in directory.iterdir() if item.is_dir()]

# Print the folders
for folder in folders:
    run_path = folder / '1_runs'
    if not run_path.exists():
        continue

    files = list(run_path.iterdir())
    def find_float_between_keywords(string):
        pattern = r"length_(\d+\.\d+)-distance"
        match = re.search(pattern, string)
        if match:
            return float(match.group(1))
        return None

    #temp edit
    max_files = []

    #dist_idx = []
    max_dist = 0
    for i, file in enumerate(files):
        dist = find_float_between_keywords(str(file))
        #dist_idx.append((dist, i))
        if dist > max_dist:
            max_dist = dist
            max_file = file
            max_files.append(max_file)

    #dist_idx.sort(reverse=True)

    if len(max_files) > 0:
        file = max_files[-1]
    else:
        continue

    #file = max_files[-2] if len(max_files) > 1 else max_files[-1]
#    if len(max_files) >= 6:
#        file = max_files[-6]
#    elif len(max_files) > 0:
#        file = max_files[0]
#    else:
#        continue

#    print(max_dist)
    path = np.load(file)
    print('file', file.name)

    NUM_PATH_POINTS = 900
    # Create an array of indices for the x values
    x = np.arange(path.shape[0])

    # Create a CubicSpline object for each dimension
    spline_x = CubicSpline(x, path[:, 0])
    spline_y = CubicSpline(x, path[:, 1])
    spline_z = CubicSpline(x, path[:, 2])

    # Create a new, smoother set of x values
    x_smooth = np.linspace(x.min(), x.max(), NUM_PATH_POINTS)

    # Compute the smoothed path
    path_smooth = np.stack([spline_x(x_smooth), spline_y(x_smooth), spline_z(x_smooth)], axis=1)

    # Compute the derivatives
    dx = np.gradient(path_smooth[:, 0])
    dy = np.gradient(path_smooth[:, 1])
    dz = np.gradient(path_smooth[:, 2])

    # Compute the tangent (T)
    T = np.array([dx, dy, dz])
    T /= np.linalg.norm(T, axis=0)

    # Initialize the normal and binormal arrays
    N = np.zeros_like(T)
    B = np.zeros_like(T)

    # Start with an arbitrary normal vector for the first point
    N[:, 0] = np.cross(T[:, 0], np.array([1, 0, 0]))
    if np.linalg.norm(N[:, 0]) == 0:
        N[:, 0] = np.cross(T[:, 0], np.array([0, 1, 0]))
    N[:, 0] /= np.linalg.norm(N[:, 0])

    # Start with an arbitrary binormal vector for the first point
    B[:, 0] = np.cross(T[:, 0], N[:, 0])
    B[:, 0] /= np.linalg.norm(B[:, 0])

    # For each subsequent point
    for i in range(1, path_smooth.shape[0]):
        # Compute the new tangent vector
        T[:, i] = path_smooth[i] - path_smooth[i-1]
        T[:, i] /= np.linalg.norm(T[:, i])

        # Compute the new normal vector as the previous binormal crossed with the new tangent
        N[:, i] = np.cross(B[:, i-1], T[:, i])
        N[:, i] /= np.linalg.norm(N[:, i])

        # Compute the new binormal vector as the new tangent crossed with the new normal
        B[:, i] = np.cross(T[:, i], N[:, i])
        B[:, i] /= np.linalg.norm(B[:, i])

    # The number of points to generate
    num_points = 100

    # The distance away from the path
    distance = 1

    # The array to hold the points
    points = np.zeros((path_smooth.shape[0], num_points, 3))

    # For each point along the path
    for i in range(path_smooth.shape[0]):
        # For each radial point
        for j in range(num_points):
            # Compute the angle for this point
            angle = 2 * np.pi * j / num_points

            # Compute the displacement from the path
            displacement = distance * (N[:, i] * np.cos(angle) + B[:, i] * np.sin(angle))

            # Add the displacement to the path point to get the radial point
            points[i, j] = path_smooth[i] + displacement

    output = points.reshape(NUM_PATH_POINTS, -1)
    path_csv = folder / '2_run_to_csv' / f'{file.stem}.csv'
    path_csv.parent.mkdir(parents=True, exist_ok=True)
    np.savetxt(path_csv, output, delimiter=',')
