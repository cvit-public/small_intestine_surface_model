// Including necessary libraries and namespaces
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Surface_mesh.h>
#include <CGAL/alpha_wrap_3.h>
#include <CGAL/Polygon_mesh_processing/bbox.h>
#include <CGAL/Polygon_mesh_processing/IO/polygon_mesh_io.h>
#include <CGAL/Real_timer.h>
#include <iostream>
#include <string>
#include <boost/filesystem.hpp>

namespace AW3 = CGAL::Alpha_wraps_3;
namespace PMP = CGAL::Polygon_mesh_processing;
namespace fs = boost::filesystem;
using K = CGAL::Exact_predicates_inexact_constructions_kernel;
using Point_3 = K::Point_3;
using Mesh = CGAL::Surface_mesh<Point_3>;

// Function to process a given file, generate its alpha shape, and save the result.
void process_file(const std::string& filename, const double relative_alpha, const double relative_offset, const std::string& output_dir) {
    // Reading the input mesh file
    std::cout << "Reading " << filename << "..." << std::endl;
    Mesh mesh;
    if(!PMP::IO::read_polygon_mesh(filename, mesh) || is_empty(mesh) || !is_triangle_mesh(mesh)) {
        std::cerr << "Invalid input." << std::endl;
        return;
    }

    // Displaying input mesh stats
    std::cout << "Input: " << num_vertices(mesh) << " vertices, " << num_faces(mesh) << " faces" << std::endl;

    // Computing bounding box and its diagonal length for the input mesh
    CGAL::Bbox_3 bbox = CGAL::Polygon_mesh_processing::bbox(mesh);
    const double diag_length = std::sqrt(CGAL::square(bbox.xmax() - bbox.xmin()) +
                                         CGAL::square(bbox.ymax() - bbox.ymin()) +
                                         CGAL::square(bbox.zmax() - bbox.zmin()));

    // Calculating alpha and offset values based on the bounding box diagonal length
    const double alpha = diag_length / relative_alpha;
    const double offset = diag_length / relative_offset;

    // Computing the alpha shape
    CGAL::Real_timer t;
    t.start();
    Mesh wrap;
    CGAL::alpha_wrap_3(mesh, alpha, offset, wrap);
    t.stop();

    // Displaying result stats and computation time
    std::cout << "Result: " << num_vertices(wrap) << " vertices, " << num_faces(wrap) << " faces" << std::endl;
    std::cout << "Took " << t.time() << " s." << std::endl;

    // Constructing the output file name
    std::string input_name = filename.substr(filename.find_last_of("/") + 1, filename.length() - 1);
    input_name = input_name.substr(0, input_name.find_last_of("."));
    std::string output_name = output_dir + "/" + input_name
                              + "_alpha_shape"
                              + "_" + std::to_string(static_cast<int>(relative_alpha))
                              + "_" + std::to_string(static_cast<int>(relative_offset)) + ".stl";

    // Saving the resulting alpha shape to the output file
    std::cout << "Writing to " << output_name << std::endl;
    CGAL::IO::write_polygon_mesh(output_name, wrap, CGAL::parameters::stream_precision(17));
}

int main(int argc, char** argv) {
    std::cout.precision(17);

    // Reading input arguments or using default values
    const std::string input_path = (argc > 1) ? argv[1] : CGAL::data_file_path("meshes/armadillo.off");
    const std::string output_dir = (argc > 4) ? argv[4] : fs::path(input_path).parent_path().string();
    const double relative_alpha = (argc > 2) ? std::stod(argv[2]) : 20.;
    const double relative_offset = (argc > 3) ? std::stod(argv[3]) : 600.;

    // Checking and creating the output directory if it doesn't exist
    if (!fs::exists(output_dir)) {
        fs::create_directory(output_dir);
    }

    // Processing the input file(s)
    fs::path p(input_path);
    if(fs::is_directory(p)) {
        for(fs::recursive_directory_iterator dir_iter(p); dir_iter != fs::recursive_directory_iterator(); ++dir_iter) {
            if(fs::is_regular_file(dir_iter->status()) && dir_iter->path().filename() == "small_bowel.stl") {
                // Use the parent directory of the file as the output directory
                std::string output_directory = dir_iter->path().parent_path().string();
                process_file(dir_iter->path().string(), relative_alpha, relative_offset, output_directory);
            }
        }
    } else if(fs::is_regular_file(p) && p.filename() == "small_bowel.stl") {
        // Use the parent directory of the file as the output directory
        std::string output_directory = p.parent_path().string();
        process_file(input_path, relative_alpha, relative_offset, output_directory);
    } else {
        std::cerr << "Invalid input path." << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}


