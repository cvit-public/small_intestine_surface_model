# How to build alpha_shape.cpp

```
mkdir build
cd build
cmake ..
make
```

## How to Run

```
./alpha_shape ../../data 40 17 ./petct_alpha_shapes
```
