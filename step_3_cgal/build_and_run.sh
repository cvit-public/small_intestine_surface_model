#!/bin/bash

# Get the directory of the current script
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# Build the ../alpha_shape.cpp file
cd "$SCRIPT_DIR"
mkdir build
cd build
cmake ..
make

# Run alpha shape program
./alpha_shape "$SCRIPT_DIR/../data" 40 17 "$SCRIPT_DIR/petct_alpha_shapes"
