#!/bin/bash

# Source and destination directories
SRC_DIR="/scratch/railabs/ld258/phantoms_project/output/SEG_PROJECT/DUKE_PET_CT_II/DUKESEG/XCAT2.0/individual_masks/"
DEST_DIR="./individual_masks/"

# File containing the list of cases
CASE_LIST_FILE="case_list.txt"

# Check if the case list file exists
if [ ! -f "$CASE_LIST_FILE" ]; then
    echo "Case list file $CASE_LIST_FILE does not exist."
    exit 1
fi

# Ensure the destination parent directory exists
mkdir -p "$DEST_DIR"

# Loop through each case in the file and copy it
while read -r case_name; do
    if [ -n "$case_name" ]; then  # Check if line is not empty
        if [ -d "${SRC_DIR}${case_name}" ]; then
            rsync -av "${SRC_DIR}${case_name}/" "${DEST_DIR}${case_name}/"
        else
            echo "Source folder ${SRC_DIR}${case_name} does not exist."
        fi
    fi
done < "$CASE_LIST_FILE"

