#!/bin/bash

# Get the directory of the current script
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# Default values
ALPHA=40
OFFSET=17
VOLUME_SCALE=2
MASK_DIR="$SCRIPT_DIR/individual_masks"

# Parse command-line arguments
# e.g.: ./script_name.sh -a 40 -o 17 -v 3 -m /path/to/masks
while getopts a:o:v:m: option; do
    case "${option}" in
        a) ALPHA=${OPTARG};;
        o) OFFSET=${OPTARG};;
        v) VOLUME_SCALE=${OPTARG};;
        m) MASK_DIR=${OPTARG};;
    esac
done

# Check for command success
check_command_success() {
    if [ $? -ne 0 ]; then
        echo "Error during execution of $1"
        exit 1
    fi
}

# Beginning of the script
echo "Starting the processing pipeline..."

echo $SCRIPT_DIR

# Step 1: Convert nii to stl
echo "Step 1: Converting nii to stl..."
python "$SCRIPT_DIR/step_1_nii_to_stl.py" $MASK_DIR
check_command_success "step_1_nii_to_stl.py"

# Step 2: Calculate volume
echo "Step 2: Calculating volume..."
python "$SCRIPT_DIR/step_2_calculate_volume.py" $VOLUME_SCALE
check_command_success "step_2_calculate_volume.py"

# Step 3: Run alpha shape program
echo "Step 3: Running alpha shape..."
"$SCRIPT_DIR/step_3_cgal/build_and_run.sh"
check_command_success "alpha_shape"

# Step 4: Start end points
echo "Step 4: Processing start and end points..."
python "$SCRIPT_DIR/step_4_start_end_points.py"
check_command_success "step_4_start_end_points.py"

# Step 5: Small intestine random walk
echo "Step 5: Running small intestine random walk..."
python "$SCRIPT_DIR/step_5_small_intestine_random_walk.py"
check_command_success "step_5_small_intestine_random_walk.py"

# Step 6: Convert result
echo "Step 6: Converting results..."
python "$SCRIPT_DIR/step_6_convert_result.py"
check_command_success "step_6_convert_result.py"

# Step 7: Run the grow_sm_intest_new script
echo "Step 7: Running grow_sm_intest_new script..."
"$SCRIPT_DIR/step_7_grow_sm_intest_new/run.sh" $ALPHA $OFFSET
check_command_success "run.sh"

echo "All steps completed successfully."

