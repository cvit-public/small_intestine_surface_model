# Running Instructions

## Data Preparation

Before running the script, you'll need to organize your segmentation masks as follows:

1. **Create an "individual_masks" folder:**  This folder will be the main directory holding all your individual cases.

2. **Organize masks by case ID:**
   * For each individual case, create a separate subfolder within "individual_masks". Use a unique case ID as the folder name (e.g., `patient01`, `case123`, etc.).
   * **Place the following STL files inside each case ID folder:**
      * `small_bowel.stl`
      * `colon.stl`
      * `duodenum.stl`

**Example Directory Structure:**

```
individual_masks/
  └── patient01/
      ├── small_bowel.stl
      ├── colon.stl
      └── duodenum.stl
  └── patient02/
      ├── small_bowel.stl
      ├── colon.stl
      └── duodenum.stl
  └── ... 
```

**Important:** 

- Ensure the STL file names (`small_bowel.stl`, `colon.stl`, `duodenum.stl`) are exactly as specified.
- The script's default mask directory is `individual_masks`.  You can change this using the `-m` command-line argument when running the script if needed.

Once you have prepared your data in this structure, you can proceed to run the processing script. 


## Running the Small Intestine Processing Pipeline

This script automates a multi-step processing pipeline for small intestine data. 

**Pipeline Steps:**

1. **Convert NIfTI to STL:** Converts NIfTI format masks to STL format using `step_1_nii_to_stl.py`.
2. **Calculate Volume:** Calculates volume based on a scaling factor using `step_2_calculate_volume.py`.
3. **Alpha Shape:** Generates an alpha shape representation using `step_3_cgal/build_and_run.sh`.
4. **Start/End Points:** Processes start and end points of the data using `step_4_start_end_points.py`.
5. **Small Intestine Random Walk:** Performs a random walk algorithm specifically for small intestine data using `step_5_small_intestine_random_walk.py`.
6. **Convert Results:** Converts the processed results using `step_6_convert_result.py`.
7. **Grow Small Intestine:** Runs the final `grow_sm_intest_new` program using `step_7_grow_sm_intest_new/run.sh`.

1. **Build the Docker Environment**

```
docker build -t small-intestine .
```

2. **Run the script:**
   ```bash
   docker run -v .:/root --rm -it small-intesine ./script_name.sh [-a ALPHA] [-o OFFSET] [-v VOLUME_SCALE] [-m MASK_DIR]
   ```

**Command Line Arguments:**

* `-a ALPHA`: Sets the alpha value for the alpha shape program (default: 40).
* `-o OFFSET`: Sets the offset value (default: 17).
* `-v VOLUME_SCALE`: Sets the volume scaling factor (default: 2).
* `-m MASK_DIR`: Specifies the directory containing the individual masks in NIfTI format (default: `./individual_masks`).

**Notes:**

* This script assumes all required programs and scripts are located in the specified directories relative to the main script's location.
* Error handling is implemented. The script will exit if any of the steps fail.
* Make sure to adjust the default values and script paths as needed for your specific environment. 
* Results will be saved in the `data/` folder.


